<?php
$url = 'index.php';
$id = $_POST["productToTranslate"];
$name = $_POST["translatedName"];
$price = $_POST["translatedPrice"];
$description = $_POST["translatedDescription"];

$m = new MongoClient();
$db = $m->products;
$collection = $db->id;
$cursor = $collection->find();

foreach ($cursor as $document) {
    if($id == $document["id"]){
        
        if($_COOKIE['currentLanguage'] == finnish){
            $modifiedData = array('$set' => array("finnishTranslation" => true));
            $collection->update(array("id"=>$id), $modifiedData);
            $activeJSON = "finnishproducts.json";
            $idNumber = $document["idNumber"];
        }
        else if($_COOKIE['currentLanguage'] == polish){
            $modifiedData = array('$set' => array("polishTranslation" => true));
            $collection->update(array("id"=>$id), $modifiedData);
            $activeJSON = "polishproducts.json";
            $idNumber = $document["idNumber"];
        }
    }
}
$m->close();

//Get existing product data
$existingProductDataJson = file_get_contents($activeJSON);
$productData = json_decode($existingProductDataJson, true);

$product = new StdClass();
    $product->id = $idNumber;
    $product->name = $name;
    $product->price = $price;
    $product->description = $description;

//Combine new and old product data
if($existingProductDataJson != null){
    array_push($productData, $product);
}
else{
    $productData = array($product);
}

//Turn data into JSON and push to correct file
$myJSON = json_encode($productData, JSON_PRETTY_PRINT);
file_put_contents($activeJSON, $myJSON);

header("Location: $url");
?>
