<?php
$id = $_POST["productToDelete"];
$m = new MongoClient();
$db = $m->products;
$collection = $db->id;
$url = 'index.php';
//Delete product picture
$cursor = $collection->find();
foreach ($cursor as $document) {
    if($document["id"] == $id){
        $imgToDelete = $document["image"];
        
        //Delete product from both json files
        $finnishJson = file_get_contents("finnishproducts.json");
        $polishJson = file_get_contents("polishproducts.json");
        
        $finnishProductData = json_decode($finnishJson, true);
        $polishProductData = json_decode($polishJson, true);
        
        $i=0;
        foreach($finnishProductData as $element) {
           //check the property of every element
           if($id == $element["name"]){
              unset($finnishProductData[$i]);
           }
           $i++;
        }
        
        $i=0;
        foreach($polishProductData as $element) {
           //check the property of every element
           if($id == $element["name"]){
              unset($polishProductData[$i]);
           }
           $i++;
        }
        
        $finnishJSON = json_encode($finnishProductData, JSON_PRETTY_PRINT);
        $polishJSON = json_encode($polishProductData, JSON_PRETTY_PRINT);
        
        file_put_contents("finnishproducts.json", $finnishJSON);
        file_put_contents("polishproducts.json", $polishJSON);
    }
}

unlink($imgToDelete);
//Delete product from database
$collection->remove(array('id' => $id), array("justOne" => true));

$m->close();

header("Location: $url");
?>