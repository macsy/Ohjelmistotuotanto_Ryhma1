<?php
session_start();
$cursor=0;
$search="";
?>
<!doctype html>
<!--
  Material Design Lite
  Copyright 2015 Google Inc. All rights reserved.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      https://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License
-->
<html lang="en">
  <head>
    <script>window.onload=window.a="";</script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="A front-end template that helps you build fast, modern mobile web apps.">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
    <title>Verkkokauppa.fi</title>

    <!-- Add to homescreen for Chrome on Android -->
    <meta name="mobile-web-app-capable" content="yes">
    <link rel="icon" sizes="192x192" href="images/verkkis2.png">

    <!-- Add to homescreen for Safari on iOS -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Material Design Lite">
    <link rel="apple-touch-icon-precomposed" href="images/verkkis2.png">

    <!-- Tile icon for Win8 (144x144 + tile color) -->
    <meta name="msapplication-TileImage" content="images/touch/ms-touch-icon-144x144-precomposed.png">
    <meta name="msapplication-TileColor" content="#3372DF">

    <link rel="shortcut icon" href="images/verkkis2.png">

    <!-- SEO: If your mobile URL is different from the desktop URL, add a canonical link to the desktop page https://developers.google.com/webmasters/smartphone-sites/feature-phones -->
    <!--
    <link rel="canonical" href="http://www.example.com/">
    -->

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.cyan-light_blue.min.css">
    <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.indigo-blue.min.css" /> 
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
    <script src="js/Cart.js"></script>
    <script src="js/Checkout.js"></script>
    <script src="js/Compare.js"></script>
    <script src="js/Sort.js"></script>
    <script src="js/CategoryPrintout.js"></script>
    <script src="js/money.js"></script>
    <link href="css/style.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
    <link id="colorpicker" href="css/Pink.css" rel="stylesheet">
    <link id="backgroundpicker" href="css/Dark.css" rel="stylesheet">
  </head>
  <body>
    
    <script type="text/javascript"> 
      window.onload=function(){
        fx.rates = {
          "EUR" : 1,
        	"PLN" : 4.21455 // eg. 1 USD === 0.745101 EUR
        }
        fx.base = "EUR";
        document.body.className += ' fade-out';
        var color = getCookieColor();
        if(color=="green"){
          document.getElementById("colorpicker").setAttribute('href','css/Green.css');
        }
        if(color=="blue"){
          document.getElementById("colorpicker").setAttribute('href','css/Blue.css');
        }
        if(color=="pink"){
          document.getElementById("colorpicker").setAttribute('href','css/Pink.css');
        }
        var background = getCookieBackground();
        if(background=="light"){
          document.getElementById("backgroundpicker").setAttribute('href','css/Light.css');
        }
        if(background=="dark"){
          document.getElementById("backgroundpicker").setAttribute('href','css/Dark.css');
        }
        //CURRENCY CONVERSION USING money.js
        var language = getCookieLanguage();
          if (language == "polish") {
            fx.settings = { from: "EUR", to: "PLN" };
            var pris=Math.round(parseInt(document.getElementById("price").innerHTML)* 10)/ 10;
            pris=Math.round(parseInt(fx.convert(pris)* 10))/ 10;
            document.getElementById("price").innerHTML=pris+"z\u0142";
          }
      };
      function sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
      }
      function setCookieLanguage(language) {
        document.cookie = "currentLanguage" + "=" + language + ";path=index.php";
        location.reload();
      }
      function getCookieBackground() {
          var language = "background=";
          var ca = document.cookie.split(';');
          for(var i = 0; i < ca.length; i++) {
              var c = ca[i];
              while (c.charAt(0) == ' ') {
                  c = c.substring(1);
              }
              if (c.indexOf(language) == 0) {
                  return c.substring(language.length, c.length);
              }
          }
          return "";
      }
      function getCookieColor() {
          var language = "color=";
          var ca = document.cookie.split(';');
          for(var i = 0; i < ca.length; i++) {
              var c = ca[i];
              while (c.charAt(0) == ' ') {
                  c = c.substring(1);
              }
              if (c.indexOf(language) == 0) {
                  return c.substring(language.length, c.length);
              }
          }
          return "";
      }
      function getCookieLanguage() {
          var language = "currentLanguage=";
          var ca = document.cookie.split(';');
          for(var i = 0; i < ca.length; i++) {
              var c = ca[i];
              while (c.charAt(0) == ' ') {
                  c = c.substring(1);
              }
              if (c.indexOf(language) == 0) {
                  return c.substring(language.length, c.length);
              }
          }
          return "";
      }
      
      function checkCookieLanguage() {
          var language = getCookieLanguage();
          if (language == "") {               // If language has not been set, default to finnish
              setCookieLanguage("finnish");
          }
      }
      checkCookieLanguage();
      sleep(3000);
      document.body.className += ' fade-in';
      </script>
    
    <?php // Changes all text on the page to selected language
      include ("localize.php");
      localize($_COOKIE["currentLanguage"]);
    ?>
    
    </div>
    <div class="demo-layout mdl-layout mdl-js-layout mdl-layout--fixed-drawer mdl-layout--fixed-header">
      <header class="demo-header mdl-layout__header mdl-color--blue-900 mdl-color-text--white">
        <div class="mdl-layout__header-row mdl-color--blue-grey-900">
          <script>
          function resetCookie(){
            document.cookie='cookieName=""';
            window.location.replace("index.php");}
          </script>
          
          <div class="mdl-layout-spacer"></div>
          <img class="img-rounded" src="images/suomenlippu.png" style="cursor: pointer;" alt="Finnish flag" onclick='setCookieLanguage("finnish"); reloadPage()'>
          <div class="mdl-layout-spacer"></div>
          <img class="img-rounded" src="images/puolanlippu.png" style="cursor: pointer;" alt="Polish flag" onclick='setCookieLanguage("polish"); reloadPage()'>
          
          <!-- Following buttons are only shown if the user is logged in -->
          <?php //if($_SESSION["loggedin"]==true){ ?>
          
          <?php
            $m = new MongoClient();
            $db = $m->users;
            $collection = $db->id;
            $cursor = $collection->find();
            $adminRights = false;
            foreach ($cursor as $document) {
              if($document["email"] == $_SESSION["email"]){
                if($document["adminrights"]=="true"){
                  $adminRights = true;  
                }
              }
            }
            $m->close();
            
            if($_SESSION["loggedin"]==true and $adminRights==true){
          ?>
          
          <!-- Contains all product management -->
            <div class="mdl-layout-spacer"></div>
            <button id="addProductBtn" class="mdl-button jello mdl-js-button mdl-button--raised mdl-button--colored" data-toggle="collapse" data-target="#ProductDiv"> <?php echo $UIproductsBtn ?> </button>
            <div id="ProductDiv" class="collapse mdl-navigation mdl-color--blue-900" max-width="800px">
              <div class="panel-group" id="accordion">
                  <div class="panel-heading mdl-color--blue-900 mdl-color-text--white mdl-js-button mdl-button--raised mdl-button--colored">
                    <h4 class="panel-title">
                      <a href="index.php?modify=add"> <?php echo $UIaddProductBtn ?> </a>
                    </h4>
                  </div>
                <div class="panel panel-default">
                  <div class="panel-heading mdl-color--blue-900 mdl-color-text--white mdl-js-button mdl-button--raised mdl-button--colored">
                    <h4 class="panel-title">
                      <a href="index.php?modify=modify"> <?php echo $UImodifyProductBtn ?> </a>
                    </h4>
                  </div>
                </div>
                <div class="panel panel-default">
                  <div class="panel-heading mdl-color--blue-900 mdl-color-text--white mdl-js-button mdl-button--raised mdl-button--colored">
                    <h4 class="panel-title">
                      <a href="index.php?modify=remove"> <?php echo $UIdeleteProductBtn ?> </a>
                    </h4>
                  </div>
                </div>
                
                
                <div class="panel panel-default">
                  <div class="panel-heading mdl-color--blue-900 mdl-color-text--white mdl-js-button mdl-button--raised mdl-button--colored">
                    <h4 class="panel-title">
                      <a href="index.php?modify=translate"><?php echo $UIAddTranslation?></a>
                    </h4>
                  </div>
                </div>
                
                
              </div> 
            </div>
              
            <div class="mdl-layout-spacer"></div>
            <button class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored" data-toggle="collapse" data-target="#AddCategory"><?php echo $UIaddCategoryBtn ?></button>
            <div id="AddCategory" class="collapse mdl-navigation mdl-color--blue-900" max-width="800px">
                <div class="mdl-color--blue-900 mdl-grid">
                  <form class="mdl-color--blue-900" action="createcategory.php" method="post" enctype="multipart/form-data">
                    <div class="mdl-grid">
                    <h5 class="mdl-cell--4-col"><?php echo $UIHeadCategory?></h5>
                    <div class="mdl-layout-spacer"></div>
                    <input name="HeadCategory" class="mdl-cell--8-col btn btn-primary" type="text" name="Head Category"/></div>
                    <div class="mdl-grid">
                    <h5><?php echo $UIsubCategory1Text ?>:</h5>
                    <div class="mdl-layout-spacer"></div>
                    <input name="SubCategory0" class="mdl-cell--8-col btn btn-primary" type="text" name="Sub Category0"/></div>
                    <div class="mdl-grid">
                    <h5><?php echo $UIsubCategory2Text ?>:</h5>
                    <div class="mdl-layout-spacer"></div>
                    <input name="SubCategory1" class="mdl-cell--8-col btn btn-primary" type="text" name="Sub Category1"/></div>
                    <div class="mdl-grid">
                    <div class="mdl-layout-spacer"></div>
                    <button id="AddNewCategory" class="mdl-cell--12-col btn btn-primary" type="submit" name="AddNewCategory"> <?php echo $UIaddCategoryBtn ?> </button>
                    <div class="mdl-layout-spacer"></div></div>
                    <div class="mdl-grid">
                    <button id="clearlist" class="mdl-cell--12-col btn btn-primary" name="clearlist"><?php echo $UIClearList ?></button>
                    <div class="mdl-layout-spacer"></div>
                    </div>
                </form>
                </div>
            </div>
            
            <!--Collapsable button that contains admin rights management-->
            <div class="mdl-layout-spacer"></div>
            <button class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored" data-toggle="collapse" data-target="#adminsAcc"> <?php echo $UIadminsBtn ?> </button>
            <div id="adminsAcc" class="collapse mdl-navigation mdl-color--blue-900" max-width="800px">
              <div class="panel-group" id="accordion2">
                <div class="panel panel-default">
                  <div class="panel-heading mdl-color--blue-900 mdl-color-text--white mdl-js-button mdl-button--raised mdl-button--colored">
                    <h4 class="panel-title">
                      <a data-toggle="collapse" data-parent="#accordion2" href="#collapse10"> <?php echo $UIaddAdminBtn ?> </a>
                    </h4>
                  </div>
                  <div id="collapse10" class="panel-collapse collapse">
                    <div class="panel-body mdl-color--blue-900">
                      <form name="addAdminForm" action="addadmin.php" method="post">
                        <div class="mdl-grid">
                        <h5 class="mdl-cell--4-col"> <?php echo $UIaddAdminText ?>: </h5>
                        <div class="mdl-layout-spacer"></div>
                        
                        <select class="mdl-cell--8-col btn btn-primary" name="adminToAdd">
                        <?php
                          $m = new MongoClient();
                          $db = $m->users;
                          $collection = $db->id;
                          $cursor = $collection->find();
                          foreach ($cursor as $document) {
                            if($document["adminrights"]!="true"){
                              echo '<option value="'.$document["email"].'" required>'.$document["email"].'</option>';  
                            }
                          }
                          $m->close();
                        ?>
                        </select></div>
                        
                        <div class="mdl-grid">
                        <input class="mdl-cell--12-col btn btn-primary" type="submit" name="addAdmin" value="<?php echo $UIaddAdminBtn ?>"/>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
                <div class="panel panel-default">
                  <div class="panel-heading mdl-color--blue-900 mdl-color-text--white mdl-js-button mdl-button--raised mdl-button--colored">
                    <h4 class="panel-title">
                      <a data-toggle="collapse" data-parent="#accordion2" href="#collapse20"> <?php echo $UIdeleteAdminBtn ?> </a>
                    </h4>
                  </div>
                  <div id="collapse20" class="panel-collapse collapse">
                    <div class="panel-body mdl-color--blue-900">
                      <form name="removeAdminForm" action="removeadmin.php" method="post">
                        <div class="mdl-grid">
                        <h5 class="mdl-cell--4-col"> <?php echo $UIdeleteAdminText ?>: </h5>
                        <div class="mdl-layout-spacer"></div>
                        
                        <select class="mdl-cell--8-col btn btn-primary" name="adminToRemove">
                        <?php
                          $m = new MongoClient();
                          $db = $m->users;
                          $collection = $db->id;
                          $cursor = $collection->find();
                          foreach ($cursor as $document) {
                            if($document["adminrights"]=="true"){
                              echo '<option value="'.$document["email"].'" required>'.$document["email"].'</option>';
                            }
                          }
                          $m->close();
                        ?>
                        </select></div>
                        
                        <div class="mdl-grid">
                        <input class="mdl-cell--12-col btn btn-primary" type="submit" name="removeAdmin" value="<?php echo $UIdeleteAdminBtn ?>"/>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <?php } ?>
            <?php if($_SESSION["loggedin"]==true){ ?>
            
            <!--Collapsable button that contains profile information form-->
            <div class="mdl-layout-spacer"></div>
            <button id="profileBtn" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored" data-toggle="collapse" data-target="#profileDiv"> <?php echo $UIprofileBtn ?> </button>
            <div id="profileDiv" class="collapse mdl-navigation mdl-color--blue-900" max-width="800px">
                <div class="mdl-color--blue-900 mdl-grid">
                  <form class="mdl-color--blue-900" name="myForm" action="saveuserinfo.php" method="post" enctype="multipart/form-data">
                    <div class="mdl-grid">
                    <h5 class="mdl-cell--4-col"> <?php echo $UIprofileFirstnameText ?>: </h5>
                    <div class="mdl-layout-spacer"></div>
                    <input class="mdl-cell--8-col btn btn-primary" type="text" name="firstname"/></div>
                    <div class="mdl-grid">
                    <h5> <?php echo $UIprofileSurnameText ?>: </h5>
                    <div class="mdl-layout-spacer"></div>
                    <input class="mdl-cell--8-col btn btn-primary" type="text" name="surname"/></div>
                    <div class="mdl-grid">
                    <h5> <?php echo $UIprofileStreetText ?>: </h5>
                    <div class="mdl-layout-spacer"></div>
                    <input class="mdl-cell--8-col btn btn-primary" type="text" name="street"/></div>
                    <div class="mdl-grid">
                    <h5> <?php echo $UIprofileCityText ?>: </h5>
                    <div class="mdl-layout-spacer"></div>
                    <input class="mdl-cell--8-col btn btn-primary" type="text" name="city"/></div>
                    <div class="mdl-grid">
                    <h5> <?php echo $UIprofileZIPText ?>: </h5>
                    <div class="mdl-layout-spacer"></div>
                    <input class="mdl-cell--8-col btn btn-primary" type="text" name="postalcode"/></div>
                    <div class="mdl-grid">
                    <h5> <?php echo $UIprofilePhoneText ?>: </h5>
                    <div class="mdl-layout-spacer"></div>
                    <input class="mdl-cell--8-col btn btn-primary" type="text" name="phone"/></div>
                    <div class="mdl-grid">
                    <input class="mdl-cell--12-col btn btn-primary" type="submit" name="addProduct" value="<?php echo $UIprofileSubmitBtn ?>"/>
                    <div class="mdl-layout-spacer"></div>
                    <div class="mdl-grid">
                    <div class="mdl-layout-spacer"></div></div>
                    </div>
                </form>
                </div>
            </div>

            <?php } ?>
            <!-- Buttons only shown if logged in ends here -->
            
          <div class="mdl-layout-spacer"></div>
                    
          <!--Ostoskori alkaa-->
          <div id="ostoskoribadge" class="mdl-badge mdl-badge--overlap" data-badge="0"><button type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-color--red-900 mdl-color-text--white " data-toggle="collapse" data-target="#kori" onclick="getCart()"> <?php echo $UIshoppingcartBtn ?> </button></div>
            <div id="kori" class="collapse mdl-navigation mdl-color--blue-900 mdl-grid mdl-shadow--2dp">
              <table id="cart" style="width:40vh" class="mdl-color--blue-900">
              </table>
              
              <table class="mdl-color--blue-800 mdl-shadow--2dp" style="width:40vh">
                <tr><td style="width:25%" id="pricetag"></td><td style="width:25%"></td><td style="width:25%"></td><td style="width:25%"><a style="color:white" href="index.php?checkout="> <?php echo $UIshoppingcartCheckoutBtn ?> </a></td></tr>
                
              </table>
          
            </div>
          <!--Ostoskori päättyy-->
          
           <div class="mdl-layout-spacer"></div>
           <div class="mdl-layout-spacer"></div>
          <div class="mdl-dialog__content">
          <div class="mdl-layout-spacer"></div>
          <div class="mdl-textfield mdl-js-textfield mdl-textfield--expandable">
            <form name="deleteProductForm" onsubmit="setCookie()">
            <!-- S -->
            <label class="material-icons mdl-color-text--white" for="search"><?php echo $UISearch?></label>
            <div class="mdl-textfield__expandable-holder">
              <form name="deleteProductForm" onsubmit='document.cookie=document.getElementById("search").innerHTML;'>
              <input class="mdl-textfield__input" type="text" pattern="[a-zA-Z0-9-]+" id="search"/>
              <label class="mdl-textfield__label" for="search">Enter your query...</label>
              </form>
              </div>
        </div>
        <script type="text/javascript">
        function setCookie(){
          document.cookie = "cookieName="+document.getElementById("search").value; }
        </script>
      </header>
      <div class="demo-drawer mdl-layout__drawer mdl-color--blue-grey-900 mdl-color-text--blue-grey-50">
        <div class="mdl-grid">
        <div class="mdl-layout-spacer"></div>
        <a id="nameonleft" onclick='resetCookie()' class="mdl-layout-title mdl-color-text--white">Verkkokauppa.fi</a>
        <div class="mdl-layout-spacer"></div>
        </div>
        <div class="mdl-grid">
        <div class="mdl-layout-spacer"></div>
        <header class="demo-drawer-header">
          <div class="mdl-grid">
          <div class="mdl-layout-spacer"></div>
          <img src="images/verkkis2ds.png" class="demo-avatar mdl-badge" style="cursor: pointer;" onclick="location.href = 'index.php';">
          <div class="mdl-layout-spacer"></div>
          </div>
          <div class="demo-avatar-dropdown">
            <?php
            if($_SESSION["loggedin"]==true){
              echo'<span>'.$_SESSION['email'].'</span>';
             }?>
          </div>
        </header>
        </div>
        <div class="mdl-grid">
          <script>
            function pickBlue(){
            document.getElementById("colorpicker").setAttribute('href','css/Blue.css');
            document.cookie = "color=blue";
            }
            function pickPink(){
            document.getElementById("colorpicker").setAttribute('href','css/Pink.css');
            document.cookie = "color=pink";
            }
            function pickGreen(){
            document.getElementById("colorpicker").setAttribute('href','css/Green.css');
            document.cookie = "color=green";
            }
            function pickLight(){
            document.getElementById("backgroundpicker").setAttribute('href','css/Light.css');
            document.cookie = "background=light";
            }
            function pickDark(){
            document.getElementById("backgroundpicker").setAttribute('href','css/Dark.css');
            document.cookie = "background=dark";
            }
          </script>
          <div id="backgroundpickerDiv" class="mdl-grid">
            <button class="lightpicker" onclick="pickLight()"></button>
            <button class="darkpicker" onclick="pickDark()"></button>
          </div>
          <div id="colorpickerDiv" class="mdl-grid">
            <button class="bluepicker" onclick="pickBlue()"></button>
            <button class="pinkpicker" onclick="pickPink()"></button>
            <button class="greenpicker" onclick="pickGreen()"></button>
          </div>
          <div class="mdl-grid">
          <?php if($_SESSION["loggedin"]==true){ // If user is logged in, show logout button
                ?><form name="logoutbtn" action="logout.php">
                <button id="logoutbutton" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored" type="submit"> <?php echo $UIlogOutBtn ?> </button>
                </form>
                <?php } ?>
                
          <?php if(!isset($_SESSION["loggedin"])||$_SESSION["loggedin"]==false){ // If user is not logged in, show login and signup buttons
                ?>
                <button id="loginbutton" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored" data-toggle="collapse" data-target="#login"> <?php echo $UIlogInBtn ?> </button>
                <?php } ?>
          </div>
          <div class="mdl-grid">
            <div class="mdl-layout-spacer"></div>
          <?php if(!isset($_SESSION["loggedin"])||$_SESSION["loggedin"]==false){ 
                ?><button id="signupbutton" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored" data-toggle="collapse" data-target="#signup"> <?php echo $UIsignInBtn ?> </button><?php } ?>
            <div class="mdl-layout-spacer"></div>
          </div>
        </div>
        <div class="mdl-layout__header-row mdl-color--blue-grey-900"></div>
            <div id="login" class="collapse mdl-navigation mdl-color--blue-grey-900" max-width="800px">
              <div class="mdl-color--blue-grey-900 mdl-grid">
                <form name="myForm" action="login.php" method="post">
                    <input class="scalableinput btn btn-primary" type="email" name="email" id="uname" placeholder=" <?php echo $UIsignInEmailText ?> " required/>
                    <br>
                    <br>
                    <input class="scalableinput btn btn-primary" type="password" pattern="[A-Za-z0-9]*" name="password" id="pw" placeholder=" <?php echo $UIsignInPasswordText ?> " required/>
                    <br>
                    <div id="login"><input class="scalablelogin btn btn-primary" type="submit" name="login" id="loginbutton" value=" <?php echo $UIlogInBtn ?> "/></div>
                </form>
              </div>
          </div>
          <div id="signup" class="collapse mdl-navigation mdl-color--blue-900" max-width="800px">
              <div class="mdl-color--blue-900 mdl-grid">
                <form name="myForm" action="createuser.php" method="post">
                  <h3 class="scalablelogintext" style="color: white;"> <?php echo $UIsignInEmailText ?>: </h3>
                  <br>
                  <div class="toppadsmall">
                  <input class="scalableinput btn btn-primary" type="email" name="newEmail" required/> </div>
                  <br>
                  <h3 class="scalablelogintext" id="omapadding" style="color: white;"> <?php echo $UIsignInPasswordText ?>: </h3>
                  <br>
                  <div class="toppadsmall">
                  <input class="scalableinput btn btn-primary" pattern="[A-Za-z0-9]*" type="password" name="newPassword" required/></div>
                  <br>
                  <div class="toppadlarge">
                  <input class="scalablebutton btn btn-primary" type="submit" name="createUser" value=" <?php echo $UIsignInBtn ?> "/> </div>
              </form>
          </div>
        </div>
        <nav id="CategoryList" class="demo-navigation mdl-navigation mdl-color--blue-grey-800">
          <script type="text/javascript">
            function setCookieC(categoryOne){
              document.cookie = "cookieName="+categoryOne.value;
              location.reload();
            }
            </script>
            <?php
              $m = new MongoClient();
              $db = $m->category;
              $collection = $db->id;
              $cursor = $collection->find();
              foreach ($cursor as $document) {
                $value = $document["name"]."-".$document["id"]."-".$document["parent"];
                echo '<script type="text/javascript">',
                     'AddtoCategoryList("'.$document["name"].'","'.$document["id"].'","'.$document["parent"].'");',
                     '</script>';
              }
              $m->close();
              //echo '<script> SortToLists() </script>';
              echo '<script> PrintoutCategory() </script>';
            ?>
        </nav>
      </div>
      <style>
        .centered-and-cropped { object-fit: cover }
      </style>
      <main id="grid" class="mdl-layout__content mdl-color--grey-900">
        <!-- TUOTTEIDEN GRID -->
        <div class="mdl-grid demo-content" background="images/maiadark.png">
          <!-- <div class="mdl-layout-spacer"></div> -->
          <div id="itemContainer">
            
              <!-- Square card -->
              <?php
                $m = new MongoClient();
                $db = $m->products;
                $collection = $db->id;
                //TOIMIVA search, joka etsii nyt id stringiä asd
                //$sweetQuery = array('id' => 'asdasd');

                
                if(!isset($_GET["checkout"]) && !isset($_GET["modify"]))
                {
                  
                  echo '<span class="mdl-color-text--white">'.$UIsortByText.'</span> 
                                <select onchange="setTarget(value)">
                                  <option value="1">'.$UIsortByPriceText.'</option>
                                  <option value="2">'.$UIsortByNameText.'</option>
                                  <option value="3">'.$UIsortByPopularityText.'</option>
                                </select>
                                
                                <select id="ascdesc"  onchange="reOrganizeMainView(value)">
                                  <option value=9> </option>
                                  <option value=1>'.$UIsortByAscendingText.'</option>
                                  <option value=0>'.$UIsortByDescendingText.'</otion>
                                </select>';
                                
                  echo                   //<!--Späni auttaa järjestelyssä. EI muuta funktiota-->
                                          '<span id="sortBefore" style="display:none"></span>';
                  
                  
                  echo  '<div id="compDiv" class="mdl-grid mdl-cell mdl-color--blue-800 mdl-shadow--2dp mdl-color-text--white"" style="min-width:55vw; display:none">
                            
                            <h2> Tuotevertailu </h2>
                            
                            <br>
                            
                            <table id="compBox" style="width:100%">
                              <tr id="compBoxTr" style="min-width:70vw"></tr>
                            </table>
                        </div>';
                
                //Tällä piirretään desc kentässä lisätyt tägit ja luodaan muotoilua tekstille
                function readTags($string)
                {
                  $outcome = $string;
                  
                  $tags = Array("[li]","[/li]","[u]","[/u]","[b]","[/b]","[sub]","[/sub]","[em]", "[/em]");
                  $html = Array("<ul><li>","</ul>","<u>","</u>","<b>","</b>","<sub>","</sub>","<em>","</em>");
                  
                  if(strlen($outcome) > 0)
                  {
                      
                    for($i = 0; $i < count($tags); $i = $i+2)
                    {
                      if(substr_count($outcome, $tags[$i]) == substr_count($outcome,$tags[$i+1]))
                      {
                        $outcome = str_replace($tags[$i], $html[$i], $outcome);
                        $outcome = str_replace($tags[$i+1], $html[$i+1], $outcome);
                      }
                  }
                  
                    
                    
                  }
                  

                  return $outcome;
                  
                }
                  
                $sweetQuery = array('tag' => $_COOKIE['cookieName']);
                if($_COOKIE['cookieName']!=''){
                $cursor = $collection->find($sweetQuery);
                }
                else{
                  $cursor = $collection->find();
                }
                
                  $counter = 0; // Counter to create unique names for collapsed data-targets
                  
                  //Get language specific product information from json
                  if($_COOKIE['currentLanguage'] == finnish){
                    $productJSON = file_get_contents("finnishproducts.json");
                  }
                  else if($_COOKIE['currentLanguage'] == polish){
                    $productJSON = file_get_contents("polishproducts.json");
                  }
                  $productData = json_decode($productJSON, true);
                    
                 foreach ($cursor as $document) {
                   
                   // Displays product only if the product has translation for currently selected language
                   if($_COOKIE['currentLanguage'] == finnish && $document["finnishTranslation"] == true || $_COOKIE['currentLanguage'] == polish && $document["polishTranslation"] == true){
                   
                      $reviewsArray = array();
                      $reviewsArray = $document["reviews"];
                      $arrayLength = count($reviewsArray);
                      
                      $id = "item".$counter.'-'.$document["price"].'-'.$document["id"].'-'.$arrayLength;
                      
                      foreach($productData as $products){
                        if($document["idNumber"] == $products["id"]){
                          $productName = $products["name"];
                          $productPrice = $products["price"];
                          $productDescription = $products["description"];
                        }
                      }
                      
                      if ($document["rating"][1]!=null && $document["rating"][2]=!null){
                        $ave =  $document["rating"][0];}
                      else{
                        $ave = null;
                      }
                      $description = readTags($document["description"]);
                      
                      echo 
                      
                      /*
                       * Hox! Lisätään ID:seen tiedoksi myös kaikki mahdollinen minkä perusteella sitä voidaan sortata. Kun ID saadaan talteen, voidaan napata sieltä sorttauskriteeri,
                       * järjestää arrayn sisällä ID:t toivomusten mukaan ja sen jälkeen pakottaa ne ID:n perusteella myös oikeaan järjestykseen pääsivulle
                       *
                       */
                      
                      '<div  id="'.$id.'" class="mdl-grid mdl-cell mdl-color--blue-900 mdl-shadow--2dp" style="min-width:55vw;">
                        <div class="mdl-cell"><img overflow="hidden" width="200" height="200" class="centered-and-cropped" src="'.$document["image"].'"/></div>
                        <div width="100vh"><h1 class=" mdl-color-text--white">'.
                        $productName .'</h1><h4 class="mdl-cell mdl-cell--12-col mdl-color-text--white">'.
                        $productPrice .''.$UIcurrencyText.'</h4><br /><div class="mdl-grid"><button class="mdl-button mdl-js-button mdl-button--raised mdl-cell--12-col mdl-color--blue-800 mdl-color-text--white" id=\'add-to-cart\' onClick=\'addToCart("'.$document["id"].'","'.$document["price"].'")\'>'.$UIaddToCartBtn.'</button>'.
                        '<br><button class="mdl-button mdl-js-button mdl-button--raised mdl-cell--12-col mdl-color--blue-800 mdl-color-text--white" id=\'add-to-comp\' onClick=\'addToComp("'.$document["id"].'","'.$document["price"].'","'.$document["description"].'","'.$document["image"].'")\'>'.$UIaddToCompareBtn.'</button>'.
                        
                        
                        //Scriptiä tuotteiden sorttaamista varten
                        '<script> addToProductList("'.$id.'") </script>'.
                        
                        
                        //Collapsed product information and review
                        '<div class="mdl-layout-spacer"></div><button class="mdl-button mdl-js-button mdl-button--raised mdl-cell--12-col mdl-color--blue-800 mdl-color-text--white" data-toggle="collapse" data-target="#'.$counter.'">'.$UImoreInfoBtn.'</button></div>
                         </div>
                         <div id="'.$counter.'" class="collapse" style="min-width:55vw;"> 
                         <div class="mdl-grid">
                          <h4 class="mdl-cell mdl-cell--12-col mdl-color-text--white">'.$UIamountAvailableText.': '.$document["amount"].' '.$UIamountAbbreviationText.'</h4>
                          <h4 class="mdl-cell mdl-cell--12-col mdl-color-text--white">
                            
                            <div style="min-width=30wv">
                            '.$UIAttributesText.':
                            <ul>';
                              
                                foreach(explode(",",$document["attributes"]) as $attribute)
                                {
                                  $att = explode(":", $attribute);
                                  
                                  echo "<li><b>".$att[0]."</b>: ".$att[1]."</li>";
                                  
                                } 
                              
                            echo'
                            </ul>
                            
                          <div class="mdl-layout-spacer"></div>
                          
                          </h4>
                    
                          <h4 class="mdl-color-text--white">'.$UIproductDescriptionText.': '.$description.'</h4>
                          
                          </div>';
                          
                          $reviewsArray = array();
                          $reviewsArray = $document["reviews"];
                          $arrayLength = count($reviewsArray);
                          echo '<br><br><div style="margin-top:3vw;"><h3 class="mdl-color-text--white">'.$UIpostedReviewsText.':</h3></div>';
                          if($arrayLength == 0){
                            //echo '<div><h4 class="mdl-color-text--white">Tällä tuotteella ei ole vielä arvosteluja.</h4></div>';
                          }
                          else{
                            for($i=0; $i<$arrayLength; $i++){
                              echo '<div class="mdl-cell--12-col mdl-color--blue-700" style="border-radius:10px; padding:5px; margin-bottom:10px;"><h4 class="mdl-color-text--white">'.$reviewsArray[$i].'</h4></div>';
                            }
                          }
                          
                        echo 
                          '<div style="min-width:55vw;"><form name="createReview" action="createreview.php" method="post">
                            <input type="hidden" name="productToReview" value="'.$document["id"].'"></input>
                            <h3 class="mdl-color-text--white">'.$UIreviewProductText.':</h3>
                            <p>'.$ave.'</p>
                            <fieldset class="rating" id="radio">
                              <input type="radio" id="star5" name="radio5" value="5" /><label class="mdl-color-text--white" for="star5" title="Rocks!">5 stars</label>
                              <input type="radio" id="star4" name="radio4" value="4" /><label class="mdl-color-text--white" for="star4" title="Pretty good">4 stars</label>
                              <input type="radio" id="star3" name="radio3" value="3" /><label class="mdl-color-text--white" for="star3" title="Medium">3 stars</label>
                              <input type="radio" id="star2" name="radio2" value="2" /><label class="mdl-color-text--white" for="star2" title="Kinda bad">2 stars</label>
                              <input type="radio" id="star1" name="radio1" value="1" /><label class="mdl-color-text--white" for="star1" title="Sucks big time">1 star</label>
                            </fieldset>
                            <textarea class="mdl-cell--12-col mdl-button--raised mdl-color--blue-800 mdl-color-text--white" type="text" name="review" required/></textarea>
                            <input class="mdl-cell--12-col mdl-button mdl-js-button mdl-button--raised mdl-color-text--white" type="submit" name="postReview" value="'.$UIpostReviewBtn.'"/>
                          </form></div></div>
                        </div>
                      </div>';
                      $counter = $counter+1;
                    }
                   }
                   
                   

                 
                }else if(isset($_GET["checkout"]))
                {
                
                {
                  if($_SESSION["loggedin"]==true){
                    $m = new MongoClient();
                    $db = $m->users;
                    $collection = $db->id;
                    $profileInfo = $collection->findOne(array('email' => $_SESSION["email"]));
                    if($profileInfo["firstname"] != FALSE){
                      $FirstName = $profileInfo["firstname"];
                      $SurName = $profileInfo["surname"];
                      $Street = $profileInfo['street'];
                      $ZIP = $profileInfo['postalcode'];
                      $City = $profileInfo['city'];
                      $Email = $profileInfo['email'];
                      $Phone = $profileInfo['phone'];
                    }
                  
                    $m->close();
                  }
                  
                  echo 
                      '
                      <div class="mdl-cell mdl-color--blue-900 mdl-shadow--2dp mdl-cell--12-col mdl-grid mdl-color-text--white">
                       <form action="savepurchase.php" method="post">
                       <h2>'.$UIcheckoutText.'</h2>
                       <br><br>
                               <div id="ostosBoksi" style="width:100%">
                               
                               <div id="ostoskori">
                                
                                <p>'.$UIcheckoutShoppingcartText.'</p>
                                <table id="ostoslista" style="width:40vh">
                                <td>'.$UIcheckoutProductText.'</td><td>'.$UIcheckoutPriceText.'</td><td>'.$UIcheckoutAmountText.'</td>
  

    
                                </table>
                                
                                
                                <br>
                                
                                <p> '.$UIcheckoutTotalText.' <span id="hinta"></span> </p>
                                
                                </div>
                                
                                <br>
                                <br>
            

                        
                              <div id="basicInfo">
                                <p>'.$UIprofileFirstnameText.': </p><input class="omafield" type="text" placeholder="Mikko" value="'.$FirstName.'">
                                <br>
                                <p>'.$UIprofileSurnameText.': </p><input class="omafield" type="text" placeholder="Mallikas" value="'.$SurName.'">
                                <br>
                                <p>'.$UIprofileStreetText.': </p><input class="omafield" type="text" placeholder="Esimerkintie 1" value="'.$Street.'">
                                <br>
                                <p>'.$UIprofileZIPText.': </p><input class="omafield" type="text" placeholder="00123" value="'.$ZIP.'">
                                <br>
                                <p>'.$UIprofileCityText.': </p><input class="omafield" type="text" placeholder="Helsinki" value="'.$City.'">
                                <br>
                                <p>'.$UIprofileEmailText.': </p><input class="omafield" type="text" placeholder="mikko.mallikas@gmail.com" value="'.$Email.'">
                                <br>
                                <p>'.$UIprofilePhoneText.': </p><input class="omafield" type="text" placeholder="0401234567" value="'.$Phone.'">
                                <br>
                              </div>
                        
                        
            
                              <div id="maksutapa">
                                <p>'.$UIpaymentMethodText.': </p>
                                <select onchange="changeFunc(value)" name="method" class="omafield">
                                  <option value="0" class="omafield">Paypal</option>
                                  <option value="1" class="omafield">Mastercard</option>
                                  <option value="2" class="omafield">'.$UIgiftcardText.'</option>
                                </select>
                                </div>
                                
                                <br>
                              
                                
                                
                                <div id="paypal">
                                    
                                    <p>'.$UIusernameText.': </p> <input type="text" placeholder="'.$UIusernameText.'" class="omafield">
                                    <br>
                                    <p>'.$UIsignInPasswordText.': </p> <input type="text" placeholder="'.$UIsignInPasswordText.'" class="omafield">
                                    
                                    
                                </div>
                            
                                <div id="mastercard" style="display:none">
                                  
                                    <p>'.$UIcreditcardNumberText.': </p><input type="text" class="omafield">
                                    
                                </div>
                                
                                <div id="lahjakortti" style="display:none">
                                    
                                    <p>'.$UIgiftcardNumberText.': </p><input type="text" class="omafield">
                                    
                                </div>
                                
                                <div id="finished" style="display:none">
                                
                                    '.$UIthankyouText.'
                                
                                </div>
                                
                                <br>
                            </div>
                            
                            <br>
                            <br>
                            <br>
                            
                            <button id="maksunappi" style="color:black">'.$UIpurchaseBtn.'</button>
                       </div>
                       </form>
                       
                        <script>
                        
                        drawCart(getCartCookie());
                      
                        </script>';
                }
                        
                }else if($_GET["checkout"] == "done")
                {
                  
                                  
                  echo 
                      '
                      <div class="mdl-cell mdl-color--blue-900 mdl-shadow--2dp mdl-cell--12-col mdl-grid mdl-color-text--white">
                       <h2> Kassa </h2>
                       <br><br>
                               <div id="ostosBoksi" style="width:100%">
                               
                               <div id="ostoskori">

                                
                                <div id="finished">
                                
                                    Kiitos tilauksestasi!
                                
                                </div>
                                
                                <br>
                            </div>
                            
                       </div>';
                  
                }
                        
                       
                if(isset($_GET["modify"]))
                {
                  if($_GET["modify"] == "add")
                  {
                     echo 
                      '
                      <div class="mdl-cell mdl-color--blue-900 mdl-shadow--2dp mdl-cell--12-col mdl-grid mdl-color-text--white" style="min-width:45vw;">
                      <div class="mdl-grid">
                       <h2>'.$UIAddProductJS.' &gt </h2> <h2><a class="mdl-color-text--white bold underline" href="index.php">'.$UIBack.'</a></h2></div>
                       
                      <div>
                      <form name="myForm" action="addproduct.php" method="post" enctype="multipart/form-data">
                      <br><br>
                        <div class="mdl-grid">
                        <h5 class="mdl-cell--4-col">  '.$UIproductNameText.': </h5>
                        <div class="mdl-layout-spacer"></div>
                        <input class="mdl-cell--8-col btn btn-primary" type="text" name="productname" placeholder="1" required/></div>
                        <div class="mdl-grid">
                        <h5> '.$UIproductPriceText.': </h5>
                        <div class="mdl-layout-spacer"></div>
                        <input class="mdl-cell--8-col btn btn-primary" type="text" name="price" placeholder="999" required/></div>
                        <div class="mdl-grid">
                        <h5> '.$UIproductAmountText.': </h5>
                        <div class="mdl-layout-spacer"></div>
                        <input class="mdl-cell--8-col btn btn-primary" type="text" name="amount" placeholder="10" required/></div>
                        <div class="mdl-grid">
                        <h5> '.$UIproductCategoryText.': </h5>
                        <div class="mdl-layout-spacer"></div>
                        
                        <select class="mdl-cell--8-col btn btn-primary" name="productToDelete">';
                        
                          $m = new MongoClient();
                          $db = $m->category;
                          $collection = $db->id;
                          $cursor = $collection->find();
                          foreach ($cursor as $document) {
                            echo '<option value="'.$document["name"].'" required>'.$document["name"].'</option>';  
                          }
                          $m->close();

                        echo 
                        '</select></div>
                        
                        <div class="mdl-grid">
                        <h5> '.$UIproductImageText.': </h5>
                        <div class="mdl-layout-spacer"></div>
                        <label for="fileToUpload" class="mdl-cell--8-col btn btn-primary" >'.$UIImageUploadText.'</label>
                        <input style="visibility:hidden;" type="file" name="fileToUpload" id="fileToUpload"></div>
                        <div class="mdl-grid">
                        <h5> '.$UIproductDescriptionText.': </h5>
                        <div class="mdl-layout-spacer"></div>
                        <textarea cols="5" rows="1" class="mdl-cell--8-col btn btn-primary" type="text" name="description" placeholder="'.$UIproductDescriptionText.'" required/></textarea></div>
                        <div class="mdl-grid">
                        <h5> '.$UIAttributesText.': </h5>
                        <div class="mdl-layout-spacer"></div>
                        <textarea class="mdl-cell--8-col btn btn-primary" type="text" name="attributes" placeholder="CPU: Intel Core i7-7700K"/></textarea></div>
                        <div class="mdl-grid">
                        <h5> '.$UIproductTagsText.': </h5>
                        <div class="mdl-layout-spacer"></div>
                        <textarea class="btn btn-primary mdl-cell--8-col white mdl-color--blue-800" type="text" name="tags" placeholder="intel pc" required/></textarea></div>
                        <div class="mdl-grid">
                        <div class="mdl-layout-spacer"></div>
                        <input class="mdl-cell--12-col btn btn-primary bold" type="submit" name="addProduct" value="'.$UIaddProductBtn.'"/>
                        </form>
                        </div>
  
                            
                       </div>';
                  
                }else if($_GET["modify"] == "modify")
                {
                     echo 
                      '
                      <div class="mdl-cell mdl-color--blue-900 mdl-shadow--2dp mdl-cell--12-col mdl-grid mdl-color-text--white" style="min-width:45vw;">
                      <div class="mdl-grid">
                       <h2>'.$UIModProductJS.' &gt </h2> <h2><a class="mdl-color-text--white bold underline" href="index.php">'.$UIBack.'</a></h2></div>
                       
                      <div>
                        <form class="mdl-color--blue-900" name="modifyProductForm" action="modifyproduct.php" method="post" enctype="multipart/form-data">
                        <div class="mdl-grid">
                        <h5 class="mdl-cell--4-col"> '.$UIproductNameText.': </h5>
                        <div class="mdl-layout-spacer"></div>
                        
                        <select class="mdl-cell--8-col btn btn-primary" name="productToMod">';
               
                          $m = new MongoClient();
                          $db = $m->products;
                          $collection = $db->id;
                          $cursor = $collection->find();
                          foreach ($cursor as $document) {
                            echo '<option value="'.$document["id"].'" required>'.$document["id"].'</option>';  
                          }
                          $m->close();
                        
                        echo ' 
                        </select></div>
                        
                        <div class="mdl-grid">
                        <h5> '.$UIproductPriceText.': </h5>
                        <div class="mdl-layout-spacer"></div>
                        <input class="mdl-cell--8-col btn btn-primary" type="text" name="priceMod" placeholder="999"/></div>
                        <div class="mdl-grid">
                        <h5> '.$UIproductAmountText.': </h5>
                        <div class="mdl-layout-spacer"></div>
                        <input class="mdl-cell--8-col btn btn-primary" type="text" name="amountMod" placeholder="10"/></div>
                        <div class="mdl-grid">
                        <h5> '.$UIproductCategoryText.': </h5>
                        <div class="mdl-layout-spacer"></div>
                        <select class="mdl-cell--8-col btn btn-primary" name="productToDelete">';
                        

                          $m = new MongoClient();
                          $db = $m->category;
                          $collection = $db->id;
                          $cursor = $collection->find();
                          foreach ($cursor as $document) {
                            echo '<option value="'.$document["name"].'" required>'.$document["name"].'</option>';  
                          }
                          $m->close();

                        echo 
                        '</select></div>
                        <div class="mdl-grid">
                        <h5> '.$UIproductImageText.': </h5>
                        <div class="mdl-layout-spacer"></div>
                        <label for="fileToUploadMod" class="mdl-cell--8-col btn btn-primary" >'.$UIImageUploadText.'</label>
                        <input style="visibility:hidden;" type="file" name="fileToUploadMod" id="fileToUploadMod"></div>
                        <div class="mdl-grid">
                        <h5> '.$UIproductDescriptionText.': </h5>
                        <div class="mdl-layout-spacer"></div>
                        <textarea class="mdl-cell--8-col btn btn-primary" type="text" name="descriptionMod" placeholder="'.$UIproductDescriptionText.'"/></textarea></div>
                       
                        <div class="mdl-grid">
                          <h5> '.$UIAttributesText.': </h5>
                          <div class="mdl-layout-spacer"></div>
                          <textarea class="mdl-cell--8-col btn btn-primary" type="text" name="attsMod" placeholder="CPU: Intel Core i7-7700K"/></textarea>
                        </div>
                        <div class="mdl-grid">
                          <h5> '.$UIproductTagsText.': </h5>
                          <div class="mdl-layout-spacer"></div>
                          <textarea class="btn btn-primary mdl-cell--8-col mdl-color--blue-800" type="text" name="tagsMod" placeholder="intel pc"/></textarea>
                        </div>
                        <div class="mdl-layout-spacer"></div>
                        <input class="mdl-cell--12-col btn btn-primary bold" type="submit" name="modProduct" value="'.$UImodifyProductSubmitBtn.'"/>
                        </form>
                        </div>
                            
                       </div>';
                    
                }else if($_GET["modify"] == "remove")
                {
                    echo 
                      '
                      <div class="mdl-cell mdl-color--blue-900 mdl-shadow--2dp mdl-cell--12-col mdl-grid mdl-color-text--white" style="min-width:30vw;">
                       <div class="mdl-grid">
                       <h2>'.$UIDelProductJS.' &gt </h2> <h2><a class="mdl-color-text--white bold underline" href="index.php">'.$UIBack.'</a></h2></div>

                        <form name="deleteProductForm" action="deleteproduct.php" method="post">
                         <div class="mdl-grid">
                          <h5 class="mdl-cell--12-col"> '.$UIdeleteProductText.': </h5>
                          <div class="mdl-layout-spacer"></div>
                          
                          <select class="mdl-cell--12-col btn btn-primary" name="productToDelete">';
  
                          $m = new MongoClient();
                          $db = $m->products;
                          $collection = $db->id;
                          $cursor = $collection->find();
                          foreach ($cursor as $document) {
                            echo '<option value="'.$document["id"].'" required>'.$document["id"].'</option>';
                          }
                          $m->close();
  
                          
                          echo '
                          </select>
                      </div>
                        
                        <div class="mdl-grid">
                        <input class="mdl-cell--12-col btn btn-primary bold" type="submit" name="deleteProduct" value="'.$UIdeleteProductBtn.'"/>
                        </div>
                      </form>
                        </div>
  
                            
                       </div>';
                  
                }else if($_GET["modify"] == "translate")
                {
                  echo 
                    '
                    <div class="mdl-cell mdl-color--blue-900 mdl-shadow--2dp mdl-cell--12-col mdl-grid mdl-color-text--white" style="min-width:35vw;">
                     <h2> '.$UITranslationEdit.' &gt </h2> <h2><a class="mdl-color-text--white bold underline" href="index.php">'.$UIBack.'</a></h2>
                     <br>
                     <br>
                     <br>
                     <br>
                       
                      <div>
                        <form class="mdl-color--blue-900" name="translateProductForm" action="translateproduct.php" method="post" enctype="multipart/form-data">
                          <div class="mdl-grid tasaa">
                          <h5 class="mdl-cell--4-col">'.$UIproductNameText.'</h5>
                          <div class="mdl-layout-spacer"></div>
                          
                          <select class="mdl-cell--8-col btn btn-primary" name="productToTranslate" required>';
                            $m = new MongoClient();
                            $db = $m->products;
                            $collection = $db->id;
                            $cursor = $collection->find();
                            foreach ($cursor as $document) {
                              if($_COOKIE['currentLanguage'] == finnish && $document["finnishTranslation"] == false){
                                echo '<option value="'.$document["id"].'">'.$document["id"].'</option>';
                              }
                              else if($_COOKIE['currentLanguage'] == polish && $document["polishTranslation"] == false){
                                echo '<option value="'.$document["id"].'">'.$document["id"].'</option>';
                              }
                            }
                            $m->close();
                            
                            echo '
                          </select></div>
                          
                          <div class="mdl-grid tasaa">
                          <h5 class="mdl-cell--4-col">'.$UIProductNewNameText.':</h5>
                          <div class="mdl-layout-spacer"></div>
                          <input class="mdl-cell--8-col btn btn-primary" type="text" name="translatedName"/></div>
                          
                          <div class="mdl-grid tasaa">
                          <h5 class="mdl-cell--4-col">'.$UIproductPriceText.':</h5>
                          <div class="mdl-layout-spacer"></div>
                          <input class="mdl-cell--8-col btn btn-primary" type="text" name="translatedPrice"/></div>
                          
                          <div class="mdl-grid tasaa">
                          <h5 class="mdl-cell--4-col">'.$UIproductDescriptionText.':</h5>
                            <div class="mdl-layout-spacer"></div>
                            <textarea class="mdl-cell--8-col btn btn-primary" type="text" name="translatedDescription"/></textarea>
                          </div>
                          
                          <div class="mdl-grid tasaa">
                          <div class="mdl-layout-spacer"></div>
                          <input class="mdl-cell--12-col btn btn-primary" type="submit" name="translateProduct" value="'.$UImodifyProductSubmitBtn.'"/>
                          <div class="mdl-layout-spacer"></div>
                          <div class="mdl-grid">
                          <div class="mdl-layout-spacer"></div></div>
                          </div>
                        </form>
                      </div>
                    </div>';
                }
              }
  

                 $m->close();
                 echo $search;
                 
                 
                 ?>
          </div>
         <!-- <div class="mdl-layout-spacer"></div> -->
        </div>
      </main>
    </div>
      <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" style="position: fixed; left: -1000px; height: -1000px;">
        <defs>
          <mask id="piemask" maskContentUnits="objectBoundingBox">
            <circle cx=0.5 cy=0.5 r=0.49 fill="white" />
            <circle cx=0.5 cy=0.5 r=0.40 fill="black" />
          </mask>
          <g id="piechart">
            <circle cx=0.5 cy=0.5 r=0.5 />
            <path d="M 0.5 0.5 0.5 0 A 0.5 0.5 0 0 1 0.95 0.28 z" stroke="none" fill="rgba(255, 255, 255, 0.75)" />
          </g>
        </defs>
      </svg>
      <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 500 250" style="position: fixed; left: -1000px; height: -1000px;">
        <defs>
          <g id="chart">
            <g id="Gridlines">
              <line fill="#888888" stroke="#888888" stroke-miterlimit="10" x1="0" y1="27.3" x2="468.3" y2="27.3" />
              <line fill="#888888" stroke="#888888" stroke-miterlimit="10" x1="0" y1="66.7" x2="468.3" y2="66.7" />
              <line fill="#888888" stroke="#888888" stroke-miterlimit="10" x1="0" y1="105.3" x2="468.3" y2="105.3" />
              <line fill="#888888" stroke="#888888" stroke-miterlimit="10" x1="0" y1="144.7" x2="468.3" y2="144.7" />
              <line fill="#888888" stroke="#888888" stroke-miterlimit="10" x1="0" y1="184.3" x2="468.3" y2="184.3" />
            </g>
            <g id="Numbers">
              <text transform="matrix(1 0 0 1 485 29.3333)" fill="#888888" font-family="'Roboto'" font-size="9">500</text>
              <text transform="matrix(1 0 0 1 485 69)" fill="#888888" font-family="'Roboto'" font-size="9">400</text>
              <text transform="matrix(1 0 0 1 485 109.3333)" fill="#888888" font-family="'Roboto'" font-size="9">300</text>
              <text transform="matrix(1 0 0 1 485 149)" fill="#888888" font-family="'Roboto'" font-size="9">200</text>
              <text transform="matrix(1 0 0 1 485 188.3333)" fill="#888888" font-family="'Roboto'" font-size="9">100</text>
              <text transform="matrix(1 0 0 1 0 249.0003)" fill="#888888" font-family="'Roboto'" font-size="9">1</text>
              <text transform="matrix(1 0 0 1 78 249.0003)" fill="#888888" font-family="'Roboto'" font-size="9">2</text>
              <text transform="matrix(1 0 0 1 154.6667 249.0003)" fill="#888888" font-family="'Roboto'" font-size="9">3</text>
              <text transform="matrix(1 0 0 1 232.1667 249.0003)" fill="#888888" font-family="'Roboto'" font-size="9">4</text>
              <text transform="matrix(1 0 0 1 309 249.0003)" fill="#888888" font-family="'Roboto'" font-size="9">5</text>
              <text transform="matrix(1 0 0 1 386.6667 249.0003)" fill="#888888" font-family="'Roboto'" font-size="9">6</text>
              <text transform="matrix(1 0 0 1 464.3333 249.0003)" fill="#888888" font-family="'Roboto'" font-size="9">7</text>
            </g>
            <g id="Layer_5">
              <polygon opacity="0.36" stroke-miterlimit="10" points="0,223.3 48,138.5 154.7,169 211,88.5
              294.5,80.5 380,165.2 437,75.5 469.5,223.3 	"/>
            </g>
            <g id="Layer_4">
              <polygon stroke-miterlimit="10" points="469.3,222.7 1,222.7 48.7,166.7 155.7,188.3 212,132.7
              296.7,128 380.7,184.3 436.7,125 	"/>
            </g>
          </g>
        </defs>
      </svg>
    <script src="https://code.getmdl.io/1.3.0/material.min.js"></script>
  </body>
  
  
</html>
