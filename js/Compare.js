    var compArray = Array();
    var compArrayMaxLength = 5;

      function CompProduct (a_name, a_price, a_desc, a_img) 
      {
        this.name = a_name;
        this.price = a_price;
        this.desc = a_desc;
        this.img = a_img;
      }
      
      function compBoxVisibility(true_false) 
      {
          if(true_false)
          {
              document.getElementById("compDiv").style.display = "block";
          }
          else
          {
              document.getElementById("compDiv").style.display = "none";
          }
            
      }
      
      function addToComp(a_name, a_price, a_desc)
      {
          var tempArray = Array();
          
          if(compArray.length > 0)
          {
          
              for(var i = 0; i < compArray.length; i++)
              {
                  if(compArray[i].name == a_name)
                  {
                      console.log("Cannot compare same product twice");
                      return null;
                  }
              }
              
              
              
          }
          
          if(compArray.length != compArrayMaxLength)
          {
              compArray.push(new CompProduct(a_name, a_price, a_desc));
          }
          else
          {
              compArray.splice(0,1);
              compArray.push(new CompProduct(a_name, a_price, a_desc));
              
          }
          
          console.log("CompArray length: " + compArray.length);
          compBoxVisibility(true);
          showCompItems();
          
      }
      
      function showCompItems()
      {
          
          clearChildrenOf("compBoxTr");
          
          if(compArray.length > 0)
          {
              for(var i = 0; i < compArray.length; i++)
              {
                  createItemTD(i);
              }
          }
          
      }
      
      function createItemTD(index)
      {
          var compBox = document.getElementById("compBoxTr");
          
          var td = document.createElement("td");
          var name = document.createTextNode("Tuotenimi: " + compArray[index].name);
          var price = document.createTextNode("Hinta: " + compArray[index].price + "€");
          var desc = document.createTextNode("Tuotekuvaus: " + compArray[index].desc);
          
          var btn = document.createElement("button");
          var btnTxt = document.createTextNode("Poista vertailusta");
          btn.appendChild(btnTxt);
          btn.setAttribute("onClick", "removeFromComparison("+ index + ")")
          btn.setAttribute("class","mdl-button mdl-js-button mdl-button--raised mdl-color--red-800 mdl-color-text--white");
          
          td.style.width = "14vw";
          
          td.appendChild(name);
          td.appendChild(document.createElement("br"));
          td.appendChild(document.createElement("br"));
          td.appendChild(price);
          td.appendChild(document.createElement("br"));
          td.appendChild(document.createElement("br"));
          td.appendChild(desc);
          td.appendChild(document.createElement("br"));
          td.appendChild(document.createElement("br"));
          td.appendChild(btn);
          compBox.appendChild(td);
          
      }
      
      function clearChildrenOf(id)
      {
        var target = document.getElementById(id);
          
        while (target.firstChild) 
        {
          target.removeChild(target.firstChild);
        }
        
      }
      
      function removeFromComparison(aIndex)
      {
          compArray.splice(aIndex,1);
          showCompItems();
          
          if(compArray.length < 1) compBoxVisibility(false);
      }
