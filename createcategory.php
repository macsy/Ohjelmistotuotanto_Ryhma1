<?php

include 'validate.php';

//ota ylös formista tulleet osoitukset
$match = false;
$url = 'index.php';
$urlError = 'index.php?error=invalidInput';
$c1post = $_POST["HeadCategory"];
$c2post = $_POST["SubCategory0"];
$c3post = $_POST["SubCategory1"];
$c4post = $_POST["SubCategory2"];
$c5post = $_POST["SubCategory3"];

//Tarkistetaan injektioiden varalta
if(!validateField($c1post,"default"))     
{
        header("Location: $urlError");
        exit;
}
if(!validateField($c2post,"default"))    
{
        header("Location: $urlError");
        exit;
}
if(!validateField($c3post,"default"))   
{
        header("Location: $urlError");
        exit;
}
if(!validateField($c4post,"default"))     
{
        header("Location: $urlError");
        exit;
}
if(!validateField($c5post,"default"))
{
        header("Location: $urlError");
        exit;
}

//avataan database
$m = new MongoClient();
$db = $m->category;
$collection = $db->id;

//onko formissa ja luodaan jos on
$id = $collection->count();
$id++;
$c1 = array (
"name" => $c1post, 
"id" => $id, 
"parent" => 0);
if (empty($_POST["SubCategory0"])){
    $c2=null;
    $c3=null;
    $c4=null;
    $c5=null;
}else{
    $id++;
    $c2 = array (
    "name" => $c2post, 
    "id" => $id, 
    "parent" => $c1["id"]);
    if (empty($_POST["SubCategory1"])){
        $c3=null;
        $c4=null;
        $c5=null;
    }else{
        $id++;
        $c3 = array ("name" => $c3post, "id" => $id, "parent" => $c2["id"]);
        if (empty($_POST["SubCategory2"])){
            $c4=null;
            $c5=null;
        }else{
            $id++;
            $c4 = array ("name" => $c4post, "id" => $id, "parent" => $c3["id"]);
            if (empty($_POST["SubCategory3"])){
                $c5=null;
            }else{
                $id++;
                $c5 = array ("name" => $c5post, "id" => $id, "parent" => $c4["id"]);
                
            }
        }
    }
}

//katsotaan dublikaatit
$cursor = $collection->find();
foreach ($cursor as $document) {
    if($c1["name"] == $document["name"] && $c1["parent"] == $document["parent"]){
        //match
        if ($c2 != null){
            //alennetaan id arvoja, koska dublikaattia ei lisätä listaan
            //alennetaan parent arvoja, koska dublikaattia ei lisätä listaan
            $c2["id"] = $c2["id"]-1;
            $c2["parent"] = $document["id"];
            if ($c3 != null){
                $c3["id"] = $c3["id"]-1;
                $c3["parent"] = $c3["parent"]-1;
                if ($c4 != null){
                    $c4["id"] = $c4["id"]-1;
                    $c4["parent"] = $c4["parent"]-1;
                    if ($c5 != null){
                        $c5["id"] = $c5["id"]-1;
                        $c5["parent"] = $c5["parent"]-1;
                    }
                }
            }
        }
        $match = true;
    }
}
if ($match == false){
    $collection->insert($c1);
}
if ($c2 != null){
    $match = false;
    $cursor = $collection->find();
    foreach ($cursor as $document) {
        if($c2["name"] == $document["name"] && $c2["parent"] == $document["parent"]){
            //match
            if ($c3 != null){
                $c3["id"] = $c3["id"]-1;
                if ($c4 != null){
                    $c4["id"] = $c4["id"]-1;
                    $c4["parent"] = $c4["parent"]-1;
                    if ($c5 != null){
                        $c5["id"] = $c5["id"]-1;
                        $c5["parent"] = $c5["parent"]-1;
                    }
                }
                $c3["parent"] = $document["id"];
            }
            $match = true;
        }
    }
    if ($match == false){
        $collection->insert($c2);
    }
}
if ($c3 != null){
    $match = false;
    $cursor = $collection->find();
    foreach ($cursor as $document) {
        if($c3["name"] == $document["name"] && $c3["parent"] == $document["parent"]){
            //match
            if ($c4 != null){
                $c4["parent"] = $document["id"];
                $c4["id"] = $c4["id"]-1;
                    if ($c5 != null){
                        $c5["id"] = $c5["id"]-1;
                        $c5["parent"] = $c5["parent"]-1;
                    }
            }
            $match = true;
        }
    }
    if ($match == false){
        $collection->insert($c3);
    }
}
if ($c4 != null){
    $match = false;
    $cursor = $collection->find();
    foreach ($cursor as $document) {
        if($c4["name"] == $document["name"] && $c4["parent"] == $document["parent"]){
            //match
            if ($c5 != null){
                $c5["parent"] = $document["id"];
                $c5["id"] = $c5["id"]-1;
            }
            $match = true;
        }
    }
    if ($match == false){
        $collection->insert($c4);
    }
}
if ($c5 != null){
    $match = false;
    $cursor = $collection->find();
    foreach ($cursor as $document) {
        if($c5["name"] == $document["name"] && $c5["parent"] == $document["parent"]){
            //match
            $match = true;
        }
    }
    if ($match == false){
        $collection->insert($c5);
    }
}
var_dump($collection->count());
$cursor = $collection -> find();
//foreach ($cursor as $id => $value){
//    var_dump($value);
//}
$m->close();
header("Location: $url");
?>