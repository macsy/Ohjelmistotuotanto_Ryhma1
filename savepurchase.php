<?php
session_start();
$url = 'index.php?checkout=done';
$urlError = 'index.php?error=invalidInput';
$cookie_name = "cart";

$name;
$lastname;
$address;
$pcode;
$city;
$email;
$mobile;
$method;

$products = array();


$name = $_POST["name"];
$lastname = $_POST["lastname"];
$address = $_POST["address"];
$pcode = $_POST["pcode"];
$city = $_POST["city"];
$email = $_POST["email"];
$mobile = $_POST["mobile"];
$method = $_POST["method"];

//validointi
if(!validateField($name,"name"))
{
    header("Location: $urlError");
    exit;
}
if(!validateField($lastname,"name"))
{
    header("Location: $urlError");
    exit;
}
if(!validateField($address,"default"))
{
    header("Location: $urlError");
    exit;
}
if(!validateField($pcode,"default"))
{
    header("Location: $urlError");
    exit;
}
if(!validateField($city,"default"))
{
    header("Location: $urlError");
    exit;
}
if(!validateField($email,"email"))
{
    header("Location: $urlError");
    exit;
}
if(!validateField($mobile,"mobile"))
{
    header("Location: $urlError");
    exit;
}
if(!validateField($method,"default"))
{
    header("Location: $urlError");
    exit;
}


//Tallennetaan ostostiedot tietokantaan
if(isset($_COOKIE[$cookie_name]))
{
    
    echo "Cookie:".$_COOKIE[$cookie_name];
    echo "<br><br>";
    testi($_COOKIE[$cookie_name],$products);
    echo "<br>";
    
    for($i = 0; $i < count($products); $i++)
    {
        echo implode(",", $products[$i]);
        echo "<br>";
        
    }

    

    $m = new MongoClient();
    $db = $m->purchases;
    $collection = $db->id;
    $document = array(
    "name" => $name,
    "lastname" => $lastname,
    "address" => $address,
    "pcode" => $pcode,
    "city" => $city,
    "email" => $email,
    "mobile" => $mobile,
    "products" => $_COOKIE[$cookie_name],
    "method" => $method
    );
    
    $collection->insert($document);
    
    $m->close();
    
    updateAmounts($products);


    //header("Location: $url");
    

    
    
}

function testi($str, &$array)
{
    $string = $str;
    $pos = 0;
    $rPos = 0;
    
    for($i = 0; $i < 3; $i++)
    {
        $pos = strpos($string, ":");
        
        $string = substr($string, $pos+1, strlen($string));
        
        $rPos += $pos;
        
    }
    
    if($pos == false) $string = $str;
    else $remaining = substr($str, 0, $rPos+2);
    
    $product = $string;
  
    array_push($array, explode(":", $product));

    
    if($pos == false) return $array;
    
    testi($remaining, $array);
    
}

function modifyProduct($id, $newAmount)
{
    $m = new MongoClient();
    $db = $m->products;
    $collection = $db->id;
    // Name of the product being modified
    $prodToMod = $id;
    // Get the document that is being modified
    $cursor = $collection->find();
    foreach ($cursor as $document) 
    {
        if($prodToMod == $document["id"])
        {
            $modifiedData = array('$set' => array(
                "price" => $document["price"],
                "amount" => ($document["amount"] - $newAmount),
                "category" => $document["category"],
                "image" => $document["image"],
                "description" => $document["description"],
                "tag" => $document["tag"]));
        }
    }
    
    $collection->update(array("id"=>$id), $modifiedData);
    $m->close();

}

function updateAmounts($products)
{
    for($i = 0; $i < count($products); $i++)
    {
        $productId = $products[$i][0];
        $amount = $products[$i][2];
        
        modifyProduct($productId, $amount);
        
    }
}



?>