<?php

$shoppingCart = $_COOKIE["cart"];

class Product 
{
    public $id;
    public $amount;
}



//Haetaan keksistä tuotteet ja tuotemäärät olioihin

function getCartContents($shoppingCart)
{
    $products = array();
    
    do
    {
        $commaIndex = strpos($shoppingCart, "_");
        $colonIndex = strpos($shoppingCart, ":");
        
        if($commaIndex == null) $commaIndex = strlen($shoppingCart);
        
        $product = new Product();
        
        $productString = substr($shoppingCart, 0, $commaIndex);
        
        $id = substr($productString, 0, $colonIndex);
        
        $product->id = $id;
        
        $amount = substr($productString, $colonIndex + 1, strlen($productString) - $colonIndex);
        
        $product->amount = $amount;
        
        array_push($products, $product);
        
        $shoppingCart = substr_replace($shoppingCart, "", 0, $commaIndex + 1);
        
    }while(strlen($shoppingCart) > 0);
    
    return $products;
}

$array = getCartContents($shoppingCart);



/*foreach($array as $x) {echo $x->id;
echo ",";
}

echo "<br><br>";

*/

?>


<html>
    <head>
        
        
        <script src="js/Cart.js"></script>
        
    </head>
    <body>
        
      <script type="text/javascript">
    
       function changeFunc(value) 
       {
           var paypal = document.getElementById("paypal");
           var mastercard = document.getElementById("mastercard");
           var cash = document.getElementById("cash");
           
           var x = [paypal, mastercard, cash];
           
           for(var i = 0; i < x.length; i++)
           {
               if(parseInt(value) != i)
               {
                   x[i].style.display = "none";
               }
               else
               {
                   x[i].style.display = "block";
               }
               
           }
       }
       
       
       function purchase()
       {
           var box = document.getElementById("ostosBoksi");
           var button = document.getElementById("maksunappi");
           
           while (box.firstChild) 
            {
              box.removeChild(box.firstChild);
            }
            
            box.appendChild(document.createTextNode("Kiitos raheista!"));
            
            button.style.visibility = "hidden";
       }
       

  </script>
    
    
    
        <h1>Hue täält voit ostaa tavarat</h1>
    
        <div id="ostosBoksi">
            
            Valitse maksutapa:<br>
            <select onchange="changeFunc(value);">
              <option value="0">Paypal</option>
              <option value="1">Mastercard</option>
              <option value="2">Cash</option>
            </select>
            
            <br>
            <br>
            
            <div id="paypal">
                
                Username: <input type="text">
                <br>
                Password: <input type="text">
                
                
            </div>
        
            <div id="mastercard" style="display:none">
                
                Nimi: <input type="text">
                <br>
                Osoite: <input type="text">
                <br>
                <br>
                Kortin numero: <input type="text">
                
            </div>
            
            <div id="cash" style="display:none">
                
                Kolikoiden määrä: <input type="text">
                
            </div>
            
            <br>
        </div>
        
        
        <button id="maksunappi" onClick="purchase()">Maksa</button>
        
        
        
    </body>
</html>