<?php


$globalForbidden = array("<", ">");


//Tarkistetaan ettei ole kiellettyjä merkkejä
function forbiddenCharacters($str, $forbiddenArray)
{
    $strlen = strlen( $str );
    
    $forbidden = $forbiddenArray;
    
    if($strlen < 100)
    {
        for( $i = 0; $i <= $strlen; $i++ ) 
        {
            $char = substr( $str, $i, 1 );
            
            for($x = 0; $x < count($forbiddenArray); $x++)
            {
                if($char == $forbiddenArray[$x]) 
                {
                    echo $char." vs ".$forbiddenArray[$x]."<br>";
                    echo "bad char found: ".$char;
                    return true;
                }
            }
        }
        
    }

    echo "No html injection<br>";
    return false;
    
}

function validCharacters($str, $validCharacters)
{
    $strlen = strlen( $str );
    
    $valid = $validCharacters;
    
    if($strlen < 100)
    {
        for( $i = 0; $i <= $strlen; $i++ ) 
        {
            $char = substr( $str, $i, 1 );
            
            for($x = 0; $x < count($valid); $x++)
            {
                if($valid[$x] == $char) break;
                
                if($x + 1 == count($valid)) return false;
            }
        }
    }

    echo "Only valid values found<br>";
    return true;
    
}

//forbiddenCharacters("<>", $globalForbidden);



function validateField($str, $case)
{
    if(forbiddenCharacters($str, array("<", ">"))) return false;
    
    switch ($case) 
    {
    //Täältä edellytetään '@' jonka jälkeen '.'
    case "email": 
         
         $strlen = strlen($str);
         
         for( $i = 0; $i <= $strlen; $i++ ) 
         {
             $char = substr( $str, $i, 1 );
             
             if($char == '@')
             {
                 for( $i = $i+2; $i <= $strlen; $i++ ) 
                 {
                     $char = substr( $str, $i, 1 );
                     
                     if($char == '.') 
                     {
                         echo "Valid mail<br>";
                         return true;
                         
                     }
                 }
             }
         }

        echo "Invalid mail<br>";        
        return false;
    
    //Hyväksytään pelkät numerot, tarkistetaan numeron pituus
    case "mobile":
         
         if(strlen($str) < 10 && strlen($str) < 11) 
         {
             echo "Phone number too short<br>";
             return false;
         }
         
         if(validCharacters($str, array("0","1","2","3","4","5","6","7","8","9")))
         {
            echo "Valid phone number<br>";    
            return true;
         }
         
         echo "Invalid phone number<br>"; 
         return false;
         
         
    case "number":
        
         if(validCharacters($str, array("0","1","2","3","4","5","6","7","8","9")))
         {
            echo "Valid number<br>";    
            return true;
         }
         
         echo "Not a number<br>"; 
         return false;
         
         
    case "name":
        
        if(!forbiddenCharacters($str, array("0","1","2","3","4","5","6","7","8","9"))) return true;
        
        return false;
        
    default:
         return true;
    }
    
    
}


validateField("1234567890","mobile");




?>