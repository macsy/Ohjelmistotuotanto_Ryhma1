<?php

include 'validate.php';

$target_dir = "images/";
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 1;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
$url = 'index.php';
$urlError = 'index.php?error=invalidInput';
// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
    if($check !== false) {
        //echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        //echo "File is not an image.";
        ?><script>alert("File is not an image.");</script><?php
        $uploadOk = 0;
    }
}
// Check if file already exists
if (file_exists($target_file)) {
    //echo "Sorry, file already exists.";
    $uploadOk = 0;
}
// Check file size
if ($_FILES["fileToUpload"]["size"] > 500000) {
    //echo "Sorry, your file is too large.";
    $uploadOk = 0;
}
// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg") {
    //echo "Sorry, only JPG, JPEG & PNG files are allowed.";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    ?><script>alert("There was an error adding the product");</script><?php
    header("Location: $url");
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
        //Get user input from the addproduct form
        $name = $_POST["productname"];
        $price = $_POST["price"];
        $amount = $_POST["amount"];
        $category = $_POST["kategoria"];
        $desc = $_POST["description"];
        $tags = $_POST["tags"];
        $ratingArray = array($average, $total, $votes);
        $attributes = $_POST["attributes"];
        
        //Validointi
        if(!validateField($name, "default"))
        {
            header("Location: $urlError");
            exit;
        }
        
        if(!validateField($price,"number"))
        {
            header("Location: $urlError");
            exit;
        }
        
        if(!validateField($amount,"number"))
        {
            header("Location: $urlError");
            exit;
        }
        
        if(!validateField($category,"default"))
        {
            header("Location: $urlError");
            exit;
        }
        
        if(!validateField($desc,"default"))
        {
            header("Location: $urlError");
            exit;
        }
        
        
        if(!validateField($tags,"default"))
        {
            header("Location: $urlError");
            exit;
        }
        
        if(!validateField($attributes,"default"))
        {
            header("Location: $urlError");
            exit;
        }
        
        $idNumber = 1;
        
        $m = new MongoClient();
        $db = $m->products;
        $collection = $db->id;
        $cursor = $collection->find();
        foreach ($cursor as $document) {    // Sets idNumber to be next free product ID
            $idNumber = $document["idNumber"]+1;  
        }
        $m->close();
        
        if($_COOKIE['currentLanguage'] == finnish){
            $activeJSON = "finnishproducts.json";
            $finnishTranslation = true;
            $polishTranslation = false;
        }
        else if($_COOKIE['currentLanguage'] == polish){
            $activeJSON = "polishproducts.json";
            $polishTranslation = true;
            $finnishTranslation = false;
        }
        
        //Get existing product data
        $existingProductDataJson = file_get_contents($activeJSON);
        $productData = json_decode($existingProductDataJson, true);
        
        $product = new StdClass();
        $product->id = $idNumber;
        $product->name = $name;
        $product->price = $price;
        $product->description = $desc;
        
        //Combine new and old product data
        if($existingProductDataJson != null){
            array_push($productData, $product);
        }
        else{
            $productData = array($product);
        }
        
        //Turn data into JSON and push to correct file
        $myJSON = json_encode($productData, JSON_PRETTY_PRINT);
        file_put_contents($activeJSON, $myJSON);
        
        //Save tags as an array with spaces and commas stripped
        $tagsArray = preg_split("/[\s,]+/", $tags);
        $nameArray = preg_split("/[\s,]+/", $name);
        foreach($nameArray as $element){
            array_push($tagsArray, $element);}
        
        //Take product information into database
        $m = new MongoClient();
        $db = $m->products;
        $collection = $db->id;
        $document = array(
        "idNumber" => $idNumber,
        "id" => $name, 
        "price" => $price,
        "amount" => $amount,
        "category" => $category,
        "image" => $target_file,
        "description" => $desc,
        "tag" => $tagsArray,
        "rating" => $ratingArray,
        "attributes" => $attributes,
        "finnishTranslation" => $finnishTranslation,
        "polishTranslation" => $polishTranslation 
        );
        $collection->insert($document);
        $m->close();
        ?><script>alert("Product has been added.");</script><?php
        header("Location: $url");
    } else {
        ?><script>alert("Sorry, there was an error uploading your file.");</script><?php
        header("Location: $url");
    }
}
?>