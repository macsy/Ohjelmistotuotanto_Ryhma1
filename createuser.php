<?php
session_start();
include 'validate.php';





$newPassword = $_POST['newPassword'];
$newEmail = $_POST['newEmail'];

if(!validateField($newPassword,"default") && !validateField($newEmail,"email"))     
{
        header("Location: $urlError");
        exit;
}

$match=false;
$url = 'index.php';
//Check if user exists in the database
$m = new MongoClient();
$db = $m->users;
$collection = $db->id;
$cursor = $collection->find();
foreach ($cursor as $document) {
  if($document["email"] == $newEmail){
    $match = true;
    ?>
    <script>
        alert("User with the same username already exists");
    </script>
    <?php
  }
}
$m->close();
        
// If username is not taken, create account and login
if($match==false){
    $m = new MongoClient();
    $db = $m->users;
    $collection = $db->id;
    
    $userCount=0;
    $cursor = $collection->find();
    foreach ($cursor as $document) {
        $userCount++;
    }
    if($userCount==0){
        $document = array(
        "email" => $newEmail,
        "password" => $newPassword,
        "firstname" => "",
        "surname" => "",
        "street" => "",
        "city" => "",
        "postalcode" => "",
        "phone" => "",
        "adminrights" => "true"
        );
    }
    else{
        $document = array(
        "email" => $newEmail,
        "password" => $newPassword,
        "firstname" => "",
        "surname" => "",
        "street" => "",
        "city" => "",
        "postalcode" => "",
        "phone" => ""
        );
    }
    
    $collection->insert($document);
    $m->close();
    $_SESSION["loggedin"] = true;
    $_SESSION["email"] = $newEmail;
    console.log("You have logged in");
    } 
    header("Location: $url");
?>