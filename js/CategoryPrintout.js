var holdlist = Array();
var templist = Array();
var c1 = Array();
var c2 = Array();
var c3 = Array();

function AddtoCategoryList(tname, tid, tparent){
    var notSorted = true;
    var hasChild = false;
    var value = {name: tname, id: tid, parent: tparent, child: hasChild};
    holdlist.push(value);
    if (tparent == 0){
        c1.push(value);
        notSorted = false;
    }
    if (notSorted){
        for (var i = 0; i<c1.length; i++){
            if (tparent == c1[i].id){
                c1[i].child = true;
                c2.push(value);
                notSorted = false;
            }
        }
    }
    if (notSorted){
        for (var i = 0; i<c2.length; i++){
            if (tparent == c2[i].id){
                c2[i].child = true;
                c3.push(value);
                notSorted = false;
            }
        }
    }
}

function SortToLists(){
    templist=holdlist;
    for (var i=0; i<templist.length;i++){
        if (templist[i],parent == 0){
            var value = {name: templist[i].name, id: templist[i].id, parent: templist[i].parent};
            c1.push(value);
            templist.remove(i);
            i--;
        }
    }
    SortThese(c1, c2);
    SortThese(c2, c3);
}
function SortThese(a, b){
    for (var i=0; i<a.length; i++){
        for (var k=0; k<templist.length; k++){
            if (a[i].id == templist[k].parent){
                var value = {name: templist[k].name, id: templist[k].id, parent: templist[k].parent};
                b.push(value);
                templist.remove(k);
                k--;
            }
        }    
    }
}
function clearLists(){
    holdlist = null;
    templist = null;
    c1 = null;
    c2 = null;
    c3 = null;
}
function PrintoutCategory(){
    if (holdlist != null){
        var nav = document.getElementById('CategoryList');
		nav.innerHTML = '';
		for (var i = 0; i<c1.length; i++) {
		    var note = c1[i];
			var button = document.createElement('button');
	        var buttonText = document.createTextNode(note.name);
	        button.appendChild(buttonText);
	        button.setAttribute("id", note.name);
	        button.setAttribute("class", "mdl-button mdl-js-button mdl-color-text--white");
	        if (note.child){
	            button.setAttribute("data-toggle", "collapse");
	            button.setAttribute("data-target", "#"+note.id+"c");
	            
	            var div = document.createElement('div');
    	        div.setAttribute("class", "collapse mdl-navigation mdl-color--blue-grey-900");
    	        div.setAttribute("id", note.id+"c");
    	        
    	        var div2 = document.createElement('div');
    	        div2.setAttribute("class", "mdl-color--blue-grey-900");
    	        nav.appendChild(button);
    	        div.appendChild(div2);
    	        nav.appendChild(div);
    	        
    	        for (var k = 0; k<c2.length; k++) {
    	            if (c2[k].parent == c1[i].id){
            		    var note = c2[k];
            			var button = document.createElement('button');
            	        var buttonText = document.createTextNode(note.name);
            	        button.appendChild(buttonText);
            	        button.setAttribute("id", note.name);
            	        button.setAttribute("class", "mdl-button mdl-js-button mdl-color-text--white mdl-cell--12-col");
            	        if (note.child){
            	            button.setAttribute("data-toggle", "collapse");
            	            button.setAttribute("data-target", "#"+note.id+"c");
            	            
            	            var div3 = document.createElement('div');
                	        div3.setAttribute("class", "collapse mdl-navigation mdl-color--blue-grey-900");
                	        div3.setAttribute("id", note.id+"c");
                	        
                	        var div4 = document.createElement('div');
                	        div4.setAttribute("class", "mdl-color--blue-grey-900");
                	        
                	        div2.appendChild(button);
                	        div3.appendChild(div4);
                	        div2.appendChild(div3);
                	        
                	        for (var z = 0; z<c3.length; z++) {
                	            if (c3[z].parent == c2[k].id){
                        		    var note = c3[z];
                        		    var box = document.createElement("div");
                        		    
                        		    var searcher = document.createElement("span");
                        		    searcher.setAttribute("value", note.name);
                        		    searcher.setAttribute("onclick", "setCookieC(this)");
                        		    searcher.setAttribute("class", "material-icons");
                        		    
                        			var button = document.createElement('button');
                        	        var buttonText = document.createTextNode(note.name);
                        	        button.appendChild(buttonText);
                        	        button.setAttribute("id", note.id+"c");
                        	        button.setAttribute("class", "mdl-button mdl-js-button mdl-color-text--white mdl-cell--12-col");
                        	        button.setAttribute("value", note.name);
	                                button.setAttribute("onclick", "setCookieC(this)");
	                                
	                                box.appendChild(button);
	                                box.appendChild(searcher);
                        	        div4.appendChild(box);
                        		}
                	        }
            	        }else{
            	            button.setAttribute("value", note.name);
	                        button.setAttribute("onclick", "setCookieC(this)");
            	            div2.appendChild(button);
            	        }
            		}
    	        }
	        }else{
	            button.setAttribute("value", note.name);
	            button.setAttribute("onclick", "setCookieC(this)");
	            nav.appendChild(button);
	        }
		}
    }
}
