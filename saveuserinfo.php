<?php
session_start();
$url = 'index.php';
$urlError = 'index.php?error=invalidInput';
//Get user input from the userInformation form
$firstName = $_POST["firstname"];
$surName = $_POST["surname"];
$streetName = $_POST["street"];
$cityName = $_POST["city"];
$postalCode = $_POST["postalcode"];
$phoneNumber = $_POST["phone"];

//validate input
if(!validateField($firstName,"name"))     
{
        header("Location: $urlError");
        exit;
}
if(!validateField($surName,"name"))     
{
        header("Location: $urlError");
        exit;
}
if(!validateField($streetName,"default"))    
{
        header("Location: $urlError");
        exit;
}
if(!validateField($cityName,"default"))     
{
        header("Location: $urlError");
        exit;
}
if(!validateField($postalCode,"default"))    
{
        header("Location: $urlError");
        exit;
}
if(!validateField($phoneNumber,"mobile"))    
{
        header("Location: $urlError");
        exit;
}

//Take user information into database
$m = new MongoClient();
$db = $m->users;
$collection = $db->id;
$userInfo = array('$set' => array(
                "firstname" => $firstName,
                "surname" => $surName,
                "street" => $streetName,
                "city" => $cityName,
                "postalcode" => $postalCode,
                "phone" => $phoneNumber));
// Update document
$collection->update(array("email"=>$_SESSION["email"]), $userInfo);
$m->close();
header("Location: $url");
?>
