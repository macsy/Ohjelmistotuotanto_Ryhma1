<?php

include 'validate.php';

$m = new MongoClient();
$db = $m->products;
$collection = $db->id;
// Name of the product being modified
$prodToMod = $_POST["productToMod"];
// Get the document that is being modified
$cursor = $collection->find();
foreach ($cursor as $document) {
    if($prodToMod == $document["id"]){
        $documentToMod = $document;
    }
}
$priceMod = $documentToMod["price"];
$amountMod = $documentToMod["amount"];
$categoryMod = $documentToMod["category"];
$descMod = $documentToMod["description"];
$tagsMod = $documentToMod["tag"];
$attsMod = $documentToMod["attributes"];
$url = 'index.php';
$urlError = 'index.php?error=invalidInput';

// Check if the value is set in the form, if not use the value already in the database
if(!empty($_POST["priceMod"])){
    $priceMod = $_POST["priceMod"];
    
    if(!validateField($priceMod,"number")) 
    {
        header("Location: $urlError");
        exit;
        
    }
}
if(!empty($_POST["amountMod"])){
    $amountMod = $_POST["amountMod"];
    
    if(!validateField($amountMod,"number")) 
    {
        header("Location: $urlError");
        exit;
        
    }
}
if($_POST["categoryMod"] != $documentToMod["category"]){
    $categoryMod = $_POST["categoryMod"];
    
    if(!validateField($categoryMod,"default"))     
    {
        header("Location: $urlError");
        exit;
    }
}
if(!empty($_POST["descriptionMod"])){
    $descMod = $_POST["descriptionMod"];
    
    if(!validateField($descMod,"default"))
    {
        header("Location: $urlError");
        exit;
    }
}
if(!empty($_POST["tagsMod"])){
    $tagsMod = $_POST["tagsMod"];
    
    if(!validateField($tagsMod,"default"))
    {
        header("Location: $urlError");
        exit;
    }
}

if(!empty($_POST["attsMod"])){
    $attsMod = $_POST["attsMod"];
    
    if(!validateField($tagsMod,"default"))
    {
        header("Location: $urlError");
        exit;
    }
}

if(!empty($_FILES["fileToUploadMod"]["name"])){
    // Delete the old image
    $imgToDelete = $documentToMod["image"];
    unlink($imgToDelete);

    // Upload the new image
    $target_dir = "images/";
    $target_file = $target_dir . basename($_FILES["fileToUploadMod"]["name"]);
    $uploadOk = 1;
    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
    // Check if image file is a actual image or fake image
    if(isset($_POST["submit"])) {
        $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
        if($check !== false) {
            //echo "File is an image - " . $check["mime"] . ".";
            $uploadOk = 1;
        } else {
            //echo "File is not an image.";
            $uploadOk = 0;
        }
    }
    // Check if file already exists
    if (file_exists($target_file)) {
        //echo "Sorry, file already exists.";
        $uploadOk = 0;
    }
    // Check file size
    if ($_FILES["fileToUpload"]["size"] > 500000) {
        //echo "Sorry, your file is too large.";
        $uploadOk = 0;
    }
    // Allow certain file formats
    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg") {
        //echo "Sorry, only JPG, JPEG & PNG files are allowed.";
        $uploadOk = 0;
    }
    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
        //echo "Sorry, your file was not uploaded.";
        ?><script>alert("Sorry, your file was not uploaded.");</script><?php
    // if everything is ok, try to upload file
    } else {
        if (move_uploaded_file($_FILES["fileToUploadMod"]["tmp_name"], $target_file)) {
            $fileMod = $target_file;
            //echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
        } else {
            //echo "Sorry, there was an error uploading your file.";
        }
    }
} else {
    $fileMod = $documentToMod["image"];
    
}

$modifiedData = array('$set' => array(
                "price" => $priceMod,
                "amount" => $amountMod,
                "category" => $categoryMod,
                "image" => $fileMod,
                "description" => $descMod,
                "tag" => $tagsMod,
                "attributes" => $attsMod));
// Update document
$collection->update(array("id"=>$prodToMod), $modifiedData);
$m->close();
header("Location: $url");

?>
