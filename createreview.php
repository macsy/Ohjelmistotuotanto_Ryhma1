<?php

include 'validate.php';

session_start();
$url = 'index.php';
$urlError = 'index.php?error=invalidInput';
if($_SESSION["loggedin"] == true) {

    $newReview = $_POST["review"];
    $productToReview = $_POST["productToReview"];
    
    if(!validateField($newReview,"default"))    
    {
        header("Location: $urlError");
        exit;
    }
    
    $m = new MongoClient();
    $db = $m->products;
    $collection = $db->id;
    
    if (isset($_POST['radio1']) || isset($_POST['radio2']) || isset($_POST['radio3']) || isset($_POST['radio4']) || isset($_POST['radio5'])){
        if (isset($_POST['radio1'])){
            $ReviewRating = $_POST['radio1'];
        }
        if (isset($_POST['radio2'])){
            $ReviewRating = $_POST['radio2'];
        }
        if (isset($_POST['radio3'])){
            $ReviewRating = $_POST['radio3'];
        }
        if (isset($_POST['radio4'])){
            $ReviewRating = $_POST['radio4'];
        }
        if (isset($_POST['radio5'])){
            $ReviewRating = $_POST['radio5'];
        }
        $cursor = $collection->find();
        foreach ($cursor as $document) {
            if($productToReview == $document["id"]){
                $documentsr = $document;
            }
        }
        $ratingArray = $documentsr["rating"];
        $ratingArray[2]=$ratingArray[2]+1;
        $ratingArray[1]=$ratingArray[1] + $ReviewRating;
        $ratingArray[0]= round($ratingArray[1] / $ratingArray[2]);
        $collection->update(array("id" => $productToReview),
                        array('$set' => array("rating" => $ratingArray)
                        ));
    }
    
    // Get already existing reviews
    $reviewsArray = array($newReview);
    
    
    // Add new review to the array
    $reviewsArray[count($reviewsArray)] = $newReview;
    
    // Update database with new review array
    $collection->update(array("id" => $productToReview),
                        array('$push' => array("reviews" => $newReview)
                        ));
    $m->close();
    header("Location: $url");
}
else{
?>
    <script>
        alert("You have to be loggend in to post a review.");
        window.location = "index.php";
    </script>
<?php
}
?>