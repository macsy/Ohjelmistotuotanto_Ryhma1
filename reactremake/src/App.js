import React, { Component } from 'react';
import './App.css';
import { Button, Card, CardText } from 'react-mdl';
import { Dropdown } from 'react-mdl-extra';

class App extends Component {
  render() {
    return (
      <div className="App">
        <head>
          <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons"/>
        </head>
        <div id="megagrid">
          <div id="sidebar">
            <img id="usericon"
            alt="käyttäjäkuva"
            src="http://www.iconsfind.com/wp-content/uploads/2016/10/20161014_58006bff8b1de.png"/>
            <p id="emaildisplay">kauppias@verkkis.com</p>
            <div id="loginbtngrid">
<Dropdown target={
  <Button className="white" id="login">Login</Button>}
align={'tm bm'} offset={'0 0'}>
  <div className={'custom-menu'}>
    <p>pöö</p>
  </div>
</Dropdown>
<Dropdown target={
  <Button className="white" id="signup">Signup</Button>}
align={'tm bm'} offset={'0 0'}>
  <div className={'custom-menu'}>
    <p>pöö</p>
  </div>
</Dropdown>              <button className="hidden" id="logout">Logout</button>
            </div>
            <div id="categorylist">
              <ul id="categories">
                <li id="liSpacer">a</li>
                <li>ListItem</li>
              </ul>
            </div>
            <div id="BottomArea"></div>
          </div>
          <div id="topbar">
            <div id="topbuttons">
<Dropdown target={
  <Button className="white" id="products">Products</Button>
}align={'tm bm'}>
  <div className={'custom-menu'}>
    <p>pöö</p>
  </div>
</Dropdown>

<Dropdown target={
  <Button className="white" id="admins">Admins</Button>
}align={'tm bm'}>
  <div className={'custom-menu'}>
    <p>pöö</p>
  </div>
</Dropdown>
<Dropdown target={
  <Button className="white" id="profile">Profile</Button>
}align={'tm bm'}>
  <div className={'custom-menu'}>
    <p>pöö</p>
  </div>
</Dropdown>
<Dropdown target={
  <Button className="white" id="category">Categories</Button>}
align={'tm bm'} offset={'0 0'}>
  <div className={'custom-menu'}>
    <p>pöö</p>
  </div>
</Dropdown>
            </div>
            <button class="crop" id="cart"/>
          </div>
          <div id="grid">
            <ul id="productlist">
              <li class="product">Product 1</li>
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
