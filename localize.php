<?php

// TODO: Muuta ostoskorinapin alla olevat teksit käyttämään muuttujia

// Product management
global $UIproductsBtn;
global $UIaddProductBtn;
global $UIproductNameText;
global $UIproductPriceText;
global $UIproductAmounText;
global $UIproductCategoryText;
global $UIproductImageText;
global $UIproductDescriptionText;
global $UIproductTagsText;

global $UImodifyProductBtn;         // Uses same form texts as add
global $UImodifyProductSubmitBtn;

global $UIdeleteProductBtn;
global $UIdeleteProductText;
// Category management
global $UIaddCategoryBtn;
global $UIsubCategory1Text;
global $UIsubCategory2Text;
// Admin management
global $UIadminsBtn;
global $UIaddAdminBtn;
global $UIaddAdminText;

global $UIdeleteAdminBtn;
global $UIdeleteAdminText;
// Profile (same variables used in checkout)
global $UIprofileBtn;
global $UIprofileFirstnameText;
global $UIprofileSurnameText;
global $UIprofileStreetText;
global $UIprofileCityText;
global $UIprofileZIPText;
global $UIprofilePhoneText;
global $UIprofileSubmitBtn;
global $UIprofileEmailText;
// Shopping cart
global $UIshoppingcartBtn;
global $UIshoppingcartProductText;
global $UIshoppingcartAmountText;
global $UIshoppingcartPriceText;
global $UIshoppingcartCheckoutBtn;
// Logging buttons etc
global $UIlogInBtn;
global $UIlogOutBtn;
global $UIsignInBtn;
global $UIsignInEmailText;  // Email and password are also used in checkout
global $UIsignInPasswordText;
// Product buttons
global $UIaddToCartBtn;
global $UIaddToCompareBtn;
global $UImoreInfoBtn;
global $UIcurrencyText;
global $UIamountAvailableText;
global $UIamountAbbreviationText;
global $UIpostReviewBtn;
global $UIpostedReviewsText;
global $UIreviewProductText;
// Checkout
global $UIcheckoutText;
global $UIcheckoutShoppingcartText;
global $UIcheckoutProductText;
global $UIcheckoutPriceText;
global $UIcheckoutAmountText;
global $UIcheckoutTotalText;

global $UIpaymentMethodText;
global $UIusernameText;
global $UIgiftcardText;
global $UIgiftcardNumberText;
global $UIcreditcardNumberText;
global $UIpurchaseBtn;
global $UIthankyouText;
// Product sorting
global $UIsortByText;
global $UIsortByNameText;
global $UIsortByPriceText;
global $UIsortByAscendingText;
global $UIsortByDescendingText;
global $UIAttributesText;

global $UIImageUploadText;
global $UIAddProductJS;
global $UIModProductJS;
global $UIDelProductJS;
global $UIBack;
global $UISearch;
global $UIHeadCategory;
global $UIClearList;
global $CurRate;
global $UIAddTranslation;
global $UITranslationEdit;
global $UIProductNewNameText;
global $UIsortByPopularityText;
global $UIDescription;

function localize($language){
    
    if($language == "finnish"){
        // Gets JSON and decodes it for use
        $string = file_get_contents("finnish.json");
        $json_a = json_decode($string, true);
    }
    else {
        $string = file_get_contents("polish.json");
        $json_a = json_decode($string, true);
    }
    // Changes UI texts to be in the selected language stored in JSON array thingy
    $GLOBALS['UIproductsBtn'] = $json_a[0]["UI"]["products"];
    $GLOBALS['UIaddProductBtn'] = $json_a[0]["UI"]["addProduct"];
    $GLOBALS['UIproductNameText'] = $json_a[0]["UI"]["productName"];
    $GLOBALS['UIproductPriceText'] = $json_a[0]["UI"]["productPrice"];
    $GLOBALS['UIproductAmountText'] = $json_a[0]["UI"]["productAmount"];
    $GLOBALS['UIproductCategoryText'] = $json_a[0]["UI"]["productCategory"];
    $GLOBALS['UIproductImageText'] = $json_a[0]["UI"]["productImage"];
    $GLOBALS['UIproductDescriptionText'] = $json_a[0]["UI"]["productDescription"];
    $GLOBALS['UIproductTagsText'] = $json_a[0]["UI"]["productTags"];
    
    $GLOBALS['UImodifyProductBtn'] = $json_a[0]["UI"]["modifyProduct"];
    $GLOBALS['UImodifyProductSubmitBtn'] = $json_a[0]["UI"]["modifyProductSubmit"];
    
    $GLOBALS['UIdeleteProductBtn'] = $json_a[0]["UI"]["deleteProductBtn"];
    $GLOBALS['UIdeleteProductText'] = $json_a[0]["UI"]["deleteProductText"];
    
    $GLOBALS['UIaddCategoryBtn'] = $json_a[0]["UI"]["addCategory"];
    $GLOBALS['UIheadCategoryText'] = $json_a[0]["UI"]["headCategory"];
    $GLOBALS['UIsubCategory1Text'] = $json_a[0]["UI"]["subCategory1"];
    $GLOBALS['UIsubCategory2Text'] = $json_a[0]["UI"]["subCategory2"];
    $GLOBALS['UIclearListBtn'] = $json_a[0]["UI"]["clearListCategory"];
    
    $GLOBALS['UIadminsBtn'] = $json_a[0]["UI"]["admins"];
    $GLOBALS['UIaddAdminBtn'] = $json_a[0]["UI"]["addAdmin"];
    $GLOBALS['UIaddAdminText'] = $json_a[0]["UI"]["addAdminText"];
    $GLOBALS['UIdeleteAdminBtn'] = $json_a[0]["UI"]["deleteAdmin"];
    $GLOBALS['UIdeleteAdminText'] = $json_a[0]["UI"]["deleteAdminText"];
    
    $GLOBALS['UIprofileBtn'] = $json_a[0]["UI"]["profile"];
    $GLOBALS['UIprofileFirstnameText'] = $json_a[0]["UI"]["profileFirstname"];
    $GLOBALS['UIprofileSurnameText'] = $json_a[0]["UI"]["profileSurname"];
    $GLOBALS['UIprofileStreetText'] = $json_a[0]["UI"]["profileStreet"];
    $GLOBALS['UIprofileCityText'] = $json_a[0]["UI"]["profileCity"];
    $GLOBALS['UIprofileZIPText'] = $json_a[0]["UI"]["profileZIP"];
    $GLOBALS['UIprofilePhoneText'] = $json_a[0]["UI"]["profilePhone"];
    $GLOBALS['UIprofileSubmitBtn'] = $json_a[0]["UI"]["profileSubmit"];
    $GLOBALS['UIprofileEmailText'] = $json_a[0]["UI"]["profileEmail"];
    
    $GLOBALS['UIshoppingcartBtn'] = $json_a[0]["UI"]["shoppingcart"];
    $GLOBALS['UIshoppingcartProductText'] = $json_a[0]["UI"]["shoppingcartProduct"];
    $GLOBALS['UIshoppingcartAmountText'] = $json_a[0]["UI"]["shoppingcartAmount"];
    $GLOBALS['UIshoppingcartPriceText'] = $json_a[0]["UI"]["shoppingcartPrice"];
    $GLOBALS['UIshoppingcartCheckoutBtn'] = $json_a[0]["UI"]["shoppingcartCheckout"];
    
    $GLOBALS['UIlogInBtn'] = $json_a[0]["UI"]["logIn"];
    $GLOBALS['UIlogOutBtn'] = $json_a[0]["UI"]["logOut"];
    $GLOBALS['UIsignInBtn'] = $json_a[0]["UI"]["signIn"];
    $GLOBALS['UIsignInEmailText'] = $json_a[0]["UI"]["signInEmail"];
    $GLOBALS['UIsignInPasswordText'] = $json_a[0]["UI"]["signInPassword"];
    
    $GLOBALS['UIaddToCartBtn'] = $json_a[0]["UI"]["addToCart"];
    $GLOBALS['UIaddToCompareBtn'] = $json_a[0]["UI"]["addToCompare"];
    $GLOBALS['UImoreInfoBtn'] = $json_a[0]["UI"]["moreInfo"];
    $GLOBALS['UIcurrencyText'] = $json_a[0]["UI"]["currency"];
    $GLOBALS['UIamountAvailableText'] = $json_a[0]["UI"]["amountAvailable"];
    $GLOBALS['UIamountAbbreviationText'] = $json_a[0]["UI"]["amountAbbreviation"];
    $GLOBALS['UIpostReviewBtn'] = $json_a[0]["UI"]["postReviewBtn"];
    $GLOBALS['UIpostedReviewsText'] = $json_a[0]["UI"]["postedReviewsText"];
    $GLOBALS['UIreviewProductText'] = $json_a[0]["UI"]["reviewProductText"];
    
    $GLOBALS['UIcheckoutText'] = $json_a[0]["UI"]["checkoutText"];
    $GLOBALS['UIcheckoutShoppingcartText'] = $json_a[0]["UI"]["checkoutShoppingcartText"];
    $GLOBALS['UIcheckoutProductText'] = $json_a[0]["UI"]["checkoutProductText"];
    $GLOBALS['UIcheckoutPriceText'] = $json_a[0]["UI"]["checkoutPriceText"];
    $GLOBALS['UIcheckoutAmountText'] = $json_a[0]["UI"]["checkoutAmountText"];
    $GLOBALS['UIcheckoutTotalText'] = $json_a[0]["UI"]["checkoutTotalText"];
    
    $GLOBALS['UIpaymentMethodText'] = $json_a[0]["UI"]["paymentMethod"];
    $GLOBALS['UIusernameText'] = $json_a[0]["UI"]["usernameText"];
    $GLOBALS['UIgiftcardText'] = $json_a[0]["UI"]["giftcardText"];
    $GLOBALS['UIgiftcardNumberText'] = $json_a[0]["UI"]["giftcardNumberText"];
    $GLOBALS['UIcreditcardNumberText'] = $json_a[0]["UI"]["creditcardNumberText"];
    $GLOBALS['UIpurchaseBtn'] = $json_a[0]["UI"]["purchaseBtn"];
    $GLOBALS['UIthankyouText'] = $json_a[0]["UI"]["thankyouText"];
    
    $GLOBALS['UIsortByText'] = $json_a[0]["UI"]["sortByText"];
    $GLOBALS['UIsortByNameText'] = $json_a[0]["UI"]["sortByNameText"];
    $GLOBALS['UIsortByPriceText'] = $json_a[0]["UI"]["sortByPriceText"];
    $GLOBALS['UIsortByAscendingText'] = $json_a[0]["UI"]["sortByAscendingText"];
    $GLOBALS['UIsortByDescendingText'] = $json_a[0]["UI"]["sortByDescendingText"];
    $GLOBALS['UIAttributesText'] = $json_a[0]["UI"]["AttributesText"];

    $GLOBALS['UIImageUploadText'] = $json_a[0]["UI"]["ImageUploadText"];
    $GLOBALS['UIAddProductJS'] = $json_a[0]["UI"]["AddProductJS"];
    $GLOBALS['UIModProductJS'] = $json_a[0]["UI"]["ModProductJS"];
    $GLOBALS['UIDelProductJS'] = $json_a[0]["UI"]["DelProductJS"];
    $GLOBALS['UIBack'] = $json_a[0]["UI"]["Back"];
    $GLOBALS['UISearch'] = $json_a[0]["UI"]["Search"];
    $GLOBALS['UIHeadCategory'] = $json_a[0]["UI"]["HeadCategory"];
    $GLOBALS['UIClearList'] = $json_a[0]["UI"]["ClearList"];
    $GLOBALS['CurRate'] = $json_a[0]["UI"]["ConvertionRate"];
    $GLOBALS['UIAttributesText'] = $json_a[0]["UI"]["AttributesText"];
    $GLOBALS['UIAddTranslation'] = $json_a[0]["UI"]["AddTranslation"];
    $GLOBALS['UIClearList'] = $json_a[0]["UI"]["ClearList"];
    $GLOBALS['UIHeadCategory'] = $json_a[0]["UI"]["HeadCategory"];
    $GLOBALS['UITranslationEdit'] = $json_a[0]["UI"]["TranslationEdit"];
    $GLOBALS['UIProductNewNameText'] = $json_a[0]["UI"]["ProductNewNameText"];
    $GLOBALS['UIsortByPopularityText'] = $json_a[0]["UI"]["sortByPopularityText"];
    $GLOBALS['UIDescription'] = $json_a[0]["UI"]["Description"];
    
}

function buildFinnishJSON(){
    $myFile = "finnish.json";
    $arr_data = array(); 

    //Get form data
    $formdata = array(
       'UI'=>array(
          'products'=> "Tuotteet",
          'addProduct'=> "Lisää tuote",
          'productName'=> "Tuotteen nimi",
          'productPrice'=> "Hinta",
          'productAmount'=> "Lukumäärä",
          'productCategory'=> "Kategoria",
          'productImage'=> "Tuotekuva",
          'productDescription'=> "Kuvaus",
          'productTags'=> "Avainsanat",
          'modifyProduct'=> "Muokkaa tuotetta",
          'modifyProductSubmit'=> "Tallenna",
          'deleteProductBtn'=>"Poista tuote",
          'deleteProductText'=> "Poistettava tuote",
          'addCategory'=> "Lisää kategoria",
          'admins'=> "Admins",
          'addAdmin'=> "Lisää admin",
          'addAdminText'=> "Lisättävä admin",
          'deleteAdmin'=> "Poista admin",
          'deleteAdminText'=> "Poistettava admin",
          'profile'=> "Profiili",
          'profileFirstname'=> "Etunimi",
          'profileSurname'=> "Sukunimi",
          'profileStreet'=> "Katuosoite",
          'profileCity'=> "Kaupunki",
          'profileZIP'=> "Postinumero",
          'profilePhone'=> "Puhelinnumero",
          'profileSubmit'=> "Tallenna",
          'profileEmail'=> "Sähköposti",
          'shoppingcart'=> "Ostoskori",
          'shoppingcartProduct'=> "Tuote",
          'shoppingcartAmount'=> "Lukumäärä",
          'shoppingcartPrice'=> "Hinta",
          'shoppingcartCheckout'=> "Kassalle",
          'logIn'=> "Kirjaudu sisään",
          'logOut'=> "Kirjaudu ulos",
          'signIn'=> "Rekisteröidy",
          'signInEmail'=> "Sähköposti",
          'signInPassword'=> "Salasana",
          'addToCart'=> "Lisää ostoskoriin",
          'addToCompare'=> "Lisää vertailuun",
          'moreInfo'=> "Lisää infoa",
          'currency'=> "€",
          'amountAvailable'=> "Saatavilla",
          'amountAbbreviation'=> "Kpl",
          'postReviewBtn'=> "Tallenna arvostelu",
          'postedReviewsText'=> "Tuotteen arvostelut",
          'reviewProductText'=> "Arvostele tuote",
          'checkoutText'=> "Kassa",
          'checkoutShoppingcartText'=> "Ostoskori",
          'checkoutProductText'=> "Tuote",
          'checkoutPriceText'=> "Hinta(€)",
          'checkoutAmountText'=> "Määrä",
          'checkoutTotalText'=> "Yht:",
          'paymentMethod'=> "Valitse maksutapa",
          'creditcardNumberText'=> "Kortin numero",
          'giftcardText'=> "Lahjakortti",
          'giftcardNumberText'=> "Lahjakortin koodi",
          'usernameText'=> "Käyttäjätunnus",
          'purchaseBtn'=> "Maksa",
          'thankyouText'=> "Kiitos tilauksestasi",
          'sortByText'=> "Järjestä",
          'sortByNameText'=> "Nimi",
          'sortByPriceText'=> "Hinta",
          'sortByAscendingText'=> "Nouseva",
          'sortByDescendingText'=> "Laskeva",
          "AttributesText" => "Ominaisuudet",
          "subCategory1" => "Alakategoria 1",
          "subCategory2" => "Alakategoria 2",
          "searchText" => "Etsi",
          "ImageUploadText" => "Lataa kuva",
          "AddProductJS" => "Tuotteiden lisääminen",
          "ModProductJS" => "Tuotteiden muokkaus",
          "DelProductJS" => "Tuotteiden poistaminen",
          "Back" => "Takaisin",
          "Search" => "Haku",
          "HeadCategory" => "Pääkategoria",
          "ClearList" => "Tyhjennä"
          )
    );

    //Get data from existing json file
    //$jsondata = file_get_contents($myFile);

    // converts json data into array
    //$arr_data = json_decode($jsondata, true);

    // Push user data to array
    array_push($arr_data,$formdata);

    //Convert updated array to JSON
    $jsondata = json_encode($arr_data, JSON_PRETTY_PRINT);
   
    //write json data into data.json file
    file_put_contents($myFile, $jsondata);
   
    $json = json_decode($jsondata, true);
    
    $lisääTuote = $json['addproduct'];
}

function buildPolishJSON(){
    $myFile = "polish.json";
    $arr_data = array(); // create empty array

    //Get form data
    $formdata = array(
       'UI'=>array(
          'products'=> "Produkcja",
          'addProduct'=> "Więcej o produkcie",
          'productName'=> "Nazwa produktu",
          'productPrice'=> "Cena",
          'productAmount'=> "Liczba",
          'productCategory'=> "Kategoria",
          'productImage'=> "Wizerunek marki",
          'productDescription'=> "Opis",
          'productTags'=> "Słowa kluczowe",
          'modifyProduct'=> "Edit produkt",
          'modifyProductSubmit'=> "Uratować",
          'deleteProductBtn'=>"Usuń produkt",
          'deleteProductText'=> "Zostać usunięte z produktu",
          'addCategory'=> "Więcej kategorii",
          'admins'=> "Administratorów",
          'addAdmin'=> "Dodaj administratora",
          'addAdminText'=> "Dodał Admin",
          'deleteAdmin'=> "Usunąć admina",
          'deleteAdminText'=> "Usunięte przez administratora",
          'profile'=> "Profil",
          'profileFirstname'=> "Imię",
          'profileSurname'=> "Nazwisko",
          'profileStreet'=> "Ulica",
          'profileCity'=> "Miasto",
          'profileZIP'=> "Kod pocztowy",
          'profilePhone'=> "Numer telefonu",
          'profileSubmit'=> "Uratować",
          'profileEmail'=> "E-mail",
          'shoppingcart'=> "Koszyk",
          'shoppingcartProduct'=> "Produkt",
          'shoppingcartAmount'=> "Liczba",
          'shoppingcartPrice'=> "Cena",
          'shoppingcartCheckout'=> "Zamówienie",
          'logIn'=> "Zaloguj się",
          'logOut'=> "Wyloguj",
          'signIn'=> "Zaloguj",
          'signInEmail'=> "E-mail",
          'signInPassword'=> "Hasło",
          'addToCart'=> "Dodaj do koszyka",
          'addToCompare'=> "Dodaj do porównania",
          'moreInfo'=> "Więcej informacji",
          'currency'=> "zł",
          'amountAvailable'=> "Dostępny",
          'amountAbbreviation'=> "Szt",
          'postReviewBtn'=> "Zapisz recenzję",
          'postedReviewsText'=> "Opinie o produkcie",
          'reviewProductText'=> "Oceń ten produkt",
          'checkoutText'=> "Kasa",
          'checkoutShoppingcartText'=> "Koszyk",
          'checkoutProductText'=> "Produkt",
          'checkoutPriceText'=> "Cena(zł)",
          'checkoutAmountText'=> "Liczba",
          'checkoutTotalText'=> "łącznie:",
          'paymentMethod'=> "Wybierz metodę płatności",
          'creditcardNumberText'=> "Numer karty",
          'giftcardText'=> "Talon",
          'giftcardNumberText'=> "Kodu kuponu",
          'usernameText'=> "Nazwa użytkownika",
          'purchaseBtn'=> "Wątroba",
          'thankyouText'=> "Dziękujemy za zamówienie",
          'sortByText'=> "Sortować",
          'sortByNameText'=> "Nazwa",
          'sortByPriceText'=> "Cena",
          'sortByAscendingText'=> "Rosnąco",
          'sortByDescendingText'=> "Malejąco",
          "headCategory" => "Główna kategoria",
          "subCategory1" => "Podkategoria 1",
          "subCategory2" => "Podkategoria 2",
          "clearListCategory" => "Pusta lista",
          "AddProductJS" => "Dodawanie produktów",
          "ModProductJS" => "Produkty inżynierii",
          "DelProductJS" => "Usuwanie produktów",
          )
    );

    //Get data from existing json file
    //$jsondata = file_get_contents($myFile);

    // converts json data into array
    //$arr_data = json_decode($jsondata, true);

    // Push user data to array
    array_push($arr_data,$formdata);

    //Convert updated array to JSON
    $jsondata = json_encode($arr_data, JSON_PRETTY_PRINT);
   
    //write json data into data.json file
    file_put_contents($myFile, $jsondata);
   
    $json = json_decode($jsondata, true);
    
    $lisääTuote = $json['addproduct'];
}
?>
